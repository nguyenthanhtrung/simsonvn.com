!function (window, $) {
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 220) {
                $('#back-to-top').fadeIn(500);
            } else {
                $('#back-to-top').fadeOut(500);
            }

            if ($(this).scrollTop() > 690) {
                $('#products-sidebar-categories').addClass('fixed');
            } else {
                $('#products-sidebar-categories').removeClass('fixed');
            }
        });

        $("a[href='#top']").click(function () {
            $("html, body").animate({scrollTop: 0}, "slow");
            return false;
        });

        $(document).on('click', '.zoom-area .gallery .zoom-img-item', function () {
            var $this = $(this), $img = $('.zoom-area .zoom-img > img');

            $img.attr('src', $this.data('src'));
            $img.attr('xoriginal', $this.data('xoriginal'));
        });
    });
}(window, window.jQuery);