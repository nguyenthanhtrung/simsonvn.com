!function (window, $, App) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    App.onFileSelected = function (event, element) {
        var selectedFile = event.target.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            element.attr('src', event.target.result);
        };

        reader.readAsDataURL(selectedFile);
    };

    App.setThumbAspectRatio = function (ratio) {
        var width = 150, height = 150 / ratio;

        $(".image-crop > img").cropper("setAspectRatio", ratio);

        $('#img-preview').height(height);
    };

    // DOM ready
    $(function () {
        $('table').on('click', ".check_all", function () {
            var checked = $('.check_all').is(':checked');
            $(this).closest('table').find('.check_item').each(function () {
                if ($(this).is(':checked') != checked)
                    $(this).click();
            });
        });

        $('._delete_item').click(function () {
            var __this = $(this);

            if (confirm('Bạn muốn thực hiện hành động này?')) {
                var href = __this.attr('href');
                App.refresh(href);
            }

            return false;
        });

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('.input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

    }); // End DOM ready
}(window, window.jQuery, window.App); // put semicolon to prevent error when minify script