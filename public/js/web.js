var App = App || {};

!function (window, $, App) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    App.Base64Encode = function base64_encode(stringToEncode) {
        if (typeof window !== 'undefined') {
            if (typeof window.btoa !== 'undefined') {
                return window.btoa(unescape(encodeURIComponent(stringToEncode)))
            }
        } else {
            return new Buffer(stringToEncode).toString('base64')
        }

        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var o1;
        var o2;
        var o3;
        var h1;
        var h2;
        var h3;
        var h4;
        var bits;
        var i = 0;
        var ac = 0;
        var enc = '';
        var tmpArr = [];

        if (!stringToEncode) {
            return stringToEncode
        }

        stringToEncode = unescape(encodeURIComponent(stringToEncode));

        do {
            // pack three octets into four hexets
            o1 = stringToEncode.charCodeAt(i++);
            o2 = stringToEncode.charCodeAt(i++);
            o3 = stringToEncode.charCodeAt(i++);

            bits = o1 << 16 | o2 << 8 | o3;

            h1 = bits >> 18 & 0x3f;
            h2 = bits >> 12 & 0x3f;
            h3 = bits >> 6 & 0x3f;
            h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmpArr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4)
        } while (i < stringToEncode.length);

        enc = tmpArr.join('');

        var r = stringToEncode.length % 3;

        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3)
    };

    App.log = function (msg) {
        if (typeof console == 'object') {
            window.console.log(msg);
        }
    };

    App.on = function (event, element, callback) {
        /* Used jQbox-App-likeuery 1.7 event handler */
        $('body').on(event, element, function (e) {
            callback.apply(this, arguments); // Used arguments to fixed error in IE

            // e.preventDefault();
        });
    };

    App.alert = function ($msg) {
        alert($msg);
    };

    App.refresh = function (url, timeout) {
        timeout = typeof (timeout) == 'undefined' ? 0 : timeout;
        return window.setTimeout(function () {
            if (typeof (url) == 'undefined' || url === '') {
                window.location.reload()
            } else {
                window.location = url;
            }
        }, timeout);
    };

    App.showModalMessage = function (title, content) {
        var modal = $('#modal-message');

        modal.find('.modal-title').text(title);
        modal.find('.modal-body').html(content);

        modal.modal();
    };

    // DOM ready
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (window.matchMedia('(max-width: 990px)').matches) {
            var height = $('#mobile').height() + 66;

            $('body').css({'padding-top': height + 'px'});
        } else {
            $('body').css({'padding-top': $('#desktop').height() + 'px'});
        }




        /*App.on('click', '#cart-product-checkout', function () {
            var $this = $(this), data = [];

            $('.cart-shop-product-checker:checked').each(function () {
                var item = {
                    cart_id: $(this).data('cart'),
                    quantity: parseInt($('.cart-product-quantity[data-cart=' + $(this).data('cart') + ']').val()),
                    price: parseFloat($('.cart-product-price-total[data-cart=' + $(this).data('cart') + ']').text())
                };

                data.push(item);
            });

            var data_encode = App.Base64Encode(JSON.stringify(data));

            $('#cart-product-checkout-data').val(data_encode);
            $('#cart-product-checkout-form').submit();
        });*/

        App.on('click', '.item_delete', function () {
            if (confirm('Bạn muốn xóa?')) {
                var $this = $(this), $id = $this.data('id'), $url = $this.data('url');

                $.ajax({
                    url: $url,
                    type: 'post',
                    data: {
                        id: $id
                    },
                    beforeSend: function () {
                        $this.attr('disabled', 'disabled');
                    },
                    success: function (data) {
                        App.refresh();
                    }
                });
            }
        });

        $(window).scroll(function () {
            if (window.matchMedia('(max-width: 990px)').matches) {
                if ($(this).scrollTop() > 40) {
                    $('#mobile .header-top').hide();

                    $('body').css({'padding-top': '67px'});
                } else {
                    $('#mobile .header-top').show();

                    var height = $('#mobile').height() + 66;

                    $('body').css({'padding-top': height + 'px'});
                }
            } else {
                if ($(this).scrollTop() > 240) {
                    $('.top_header').hide();
                    //$('.header-support').hide();

                    $('body').css({'padding-top': '67px'});
                } else {
                    $('.top_header').show();
                    //$('.header-support').show();

                    $('body').css({'padding-top': $('#desktop').height() + 'px'});
                }
            }
        });

        /* $(document).on('click', '.menu-toggle', function () {
         $('#mobile .header-top .menu').toggle();
         });

         $(document).on('click', '.menu-toggle-2', function () {
         $('#mobile .header-middle .menu').toggle();
         });*/

        $('.menu-toggle').bind("click touchstart", function () {
            $('#mobile .header-top .menu').toggle();
        });

        $('.menu-toggle-2').bind("click", function () {
            $('#mobile .header-middle .menu').toggle();
        });
    }); // End DOM ready...
}(window, window.jQuery, window.App);