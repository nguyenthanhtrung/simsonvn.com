$(document).ready(function() {
    var alert_read = getCookie('alert_read');
    if(alert_read != '' && alert_read == 'yes') {
        $('#notify').hide();
    }
    $('.close_alert').click(function() {
        setCookie('alert_read','yes',1);
    });
});