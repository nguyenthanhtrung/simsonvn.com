<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{!! isset($page_title)?$page_title:'SIMSONVN' !!}</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800'
          rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="/themes/universal/css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="/themes/universal/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="/themes/universal/css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="/themes/universal/img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/themes/universal/img/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/universal/img/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/universal/img/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/universal/img/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/universal/img/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/universal/img/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/universal/img/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/universal/img/apple-touch-icon-152x152.png"/>
    <!-- owl carousel css -->

    <link href="/themes/universal/css/owl.carousel.css" rel="stylesheet">
    <link href="/themes/universal/css/owl.theme.css" rel="stylesheet">

    <style type="text/css">
        .customers .item img {
            -webkit-filter: none
        }

        .product .text h3 {
            font-size: 17px;
            letter-spacing: normal;
        }

        .panel.sidebar-menu ul.nav ul li a {
            padding: 8px 15px;
            font-size: 14px;
            padding-left: 36px;
        }

        .homepage.owl-carousel,
        .homepage.owl-carousel .owl-item .item {
            height: auto !important;
        }
    </style>
</head>

<body>

<div id="all">

    <header>

        <div id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="login text-left">
                            @if(!empty($system_configs['hotline']))
                                <a href="#">HOTLINE: {{$system_configs['hotline']}}</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="login email">
                            @if(!empty($system_configs['hotline']))
                                <a href="#">EMAIL: {{$system_configs['email']}}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

            <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                <div class="container" style="margin-top: 10px">
                    <div class="navbar-header">

                        <a class="navbar-brand home" href="/">
                            <img src="/img/logo.png" alt="Simsonvn" class="hidden-xs hidden-sm">
                            <img src="/img/logo.png" alt="Simsonvn"
                                 class="visible-xs visible-sm"><span
                                    class="sr-only">Universal - go to homepage</span>
                        </a>
                        <div class="navbar-buttons">
                            <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse"
                                    data-target="#navigation">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-align-justify"></i>
                            </button>
                        </div>
                    </div>

                    <div class="navbar-collapse collapse" id="navigation">

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/">Trang chủ</a></li>

                            @if(!$main_product_categories->isEmpty())
                                <li class="dropdown use-yamm yamm-fw">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                       data-delay="200">Sản phẩm <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="yamm-content">
                                                <div class="row">
                                                    @foreach($main_product_categories as $main_product_category)
                                                        <div class="col-sm-3">
                                                            <ul>
                                                                <li>
                                                                    <a href="{{\App\Models\ProductCategory::getLink($main_product_category)}}">
                                                                        <b>{{$main_product_category->name}}</b>
                                                                    </a>
                                                                </li>

                                                                @if(!$main_product_category->categories->isEmpty())
                                                                    @foreach($main_product_category->categories as $main_sub_product_category)
                                                                        <li>
                                                                            <a href="{{\App\Models\ProductCategory::getLink($main_product_category, $main_sub_product_category)}}">
                                                                                {{$main_sub_product_category->name}}
                                                                            </a>
                                                                        </li>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                            @if(!empty($post_categories))
                                @foreach($post_categories as $post_category)
                                    <li>
                                        <a href="{!! \App\Models\Helper\PostCategory::getUrl($post_category->slug) !!}">
                                            {!! $post_category->title !!}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>

                    </div>

                    <div class="collapse clearfix" id="search">

                        <form class="navbar-form" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Tìm kiếm">
                                <span class="input-group-btn">

                                <button type="submit" class="btn btn-template-main"><i
                                            class="fa fa-search"></i></button>

                            </span>
                            </div>
                        </form>

                    </div>
                </div>


            </div>
        </div>
    </header>


    @yield('content')

    <footer id="footer">
        <div class="container">
            <div class="col-md-4 col-sm-6">
                {{--<h4>Giới thiệu</h4>--}}

                {{--<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>--}}

                <h4 style="text-decoration: underline">Công ty Cổ phần SIMSON Việt Nam</h4>

                @if(!empty($system_configs['address_1']))
                    <p> Địa chỉ: {{$system_configs['address_1']}}</p>
                @endif

                @if(!empty($system_configs['address_2']))
                    <p>{{$system_configs['address_2']}}</p>
                @endif
                <p>Hotline: {!! $system_configs['hotline'] !!}</p>
                <p>Email: {{$system_configs['email']}}</p>
            </div>

            <div class="col-md-4 col-sm-6">

                <h4 style="text-decoration: underline">Bài viết</h4>

                <div class="blog-entries">
                    @if($footer_posts)
                        @foreach($footer_posts as $_post)
                            <?php $post_link = \App\Models\Helper\Post::getUrl($_post->slug, $_post->id); ?>

                            <div class="item same-height-row clearfix">
                                <div class="image same-height-always">
                                    <a href="{{$post_link}}">
                                        <img class="img-responsive" src="{{$_post->image}}" alt="{{$_post->title}}">
                                    </a>
                                </div>
                                <div class="name same-height-always">
                                    <h5><a href="{{$post_link}}">{{$_post->title}}</a></h5>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <hr class="hidden-md hidden-lg">

            </div>

            <div class="col-md-4 col-sm-6" style="padding: 0">

                <h4 style="text-decoration: underline">Fanpage</h4>

                <?php $fb_url = !empty($system_configs['facebook']) ? $system_configs['facebook'] : ''; ?>

                @if(!empty($fb_url))
                    <div class="fb-page" data-href="{{$fb_url}}" data-small-header="false"
                         data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="{{$fb_url}}" class="fb-xfbml-parse-ignore">
                            <a href="{{$fb_url}}">Simsonvn</a>
                        </blockquote>
                    </div>
                @endif

            </div>
        </div>

    </footer>

    <a id="scroll-top"><i class="fa fa-angle-double-up"></i></a>

    <div id="copyright" style="padding: 10px 0;">
        <div class="container">
            <div class="col-md-12">
                <p class="pull-left">© {!! date('Y') !!}. Copy right by SimsonVN</p>
                <p class="pull-right">Template by SimsonVN
                </p>
            </div>
        </div>
    </div>

    @if(!empty($system_configs['hotline']))
        <div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show" id="coccoc-alo-phoneIcon"
             style="right:2%; bottom:80px;">
            <a href="tel:{!! $system_configs['hotline'] !!}">
                <div class="coccoc-alo-ph-circle"></div>
                <div class="coccoc-alo-ph-circle-fill"></div>
                <div class="coccoc-alo-ph-img-circle"></div>
                <span class="phone_text">Gọi ngay: {{$system_configs['hotline']}}</span>
            </a>
        </div>
    @endif
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="/themes/universal/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script src="/themes/universal/js/jquery.cookie.js"></script>
<script src="/themes/universal/js/waypoints.min.js"></script>
<script src="/themes/universal/js/jquery.counterup.min.js"></script>
<script src="/themes/universal/js/jquery.parallax-1.1.3.js"></script>
<script src="/themes/universal/js/front.js"></script>

<!-- owl carousel -->
<script src="/themes/universal/js/owl.carousel.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 800) {
                $('#scroll-top').addClass('show');
            } else {
                $('#scroll-top').removeClass('show');
            }
        });

        $('#scroll-top').on('click', function () {
            $("html, body").animate({scrollTop: 0}, "slow");

            return false;
        });
    });
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
            d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set._.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute("charset", "utf-8");
        $.src = "https://v2.zopim.com/?6RXTdOMqlbWZnZu8uy0s1JUjksaotgRT";
        z.t = +new Date;
        $.type = "text/javascript";
        e.parentNode.insertBefore($, e)
    })(document, "script");
</script>
<!--End of Zendesk Chat Script-->
<div id="fb-root"></div>
<script async defer
        src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=886035664907863&autoLogAppEvents=1"></script>

</body>

</html>