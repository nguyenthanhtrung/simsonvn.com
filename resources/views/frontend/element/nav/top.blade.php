<div id="top" class="container">

    <div class="row">
        <div class="slide col-md-9 col-xs-12">
            <div class="carousel slide banner" id=carousel-example-captions data-ride=carousel>
                <ol class=carousel-indicators>
                    <?php
                    $i=0;
                    if(isset($banners) && !empty($banners)) {
                        foreach ($banners as $banner) {

                            ?>
                            <li data-target=#carousel-example-captions
                                data-slide-to=<?php echo $i;?> class="<?php if($i == 0) {echo 'active';}?>">

                            </li>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </ol>
                <div class="carousel-inner">
                    <?php
                        $i=0;
                        if(isset($banners) && !empty($banners)) {
                            foreach ($banners as $banner) {

                                ?>
                                <div class="item <?php if($i == 0) {echo 'active';}?>">
                                    <img alt="First slide image" src="<?php echo $banner->image_url?>">
                                </div>
                                <?php
                                $i++;
                            }
                        }
                    ?>


                </div>
            </div>
        </div>
        <div class="box-ads col-md-3 col-xs-12 pdl0">
            <?php
                if(isset($videos) && !empty($videos)) {
                    foreach ($videos as $video) {
                        if($video->position == 'banner_side') {
                                ?>
                            <div class="ads" style="height: 160px; width: 100%; margin-bottom: 10px">
                                <?php echo $video->embed_code;?>
                            </div>
                            <?php
                        }
                    }
                }
            ?>

        </div>
    </div>

    <div class="row mrt15 mobile-ads">
        <?php
            if(isset($banner_subs) && !empty($banner_subs)) {
                foreach($banner_subs as $banner_sub) {
                    ?>
                    <div class="col-md-3">
                        <div class="ads">
                            <a href="<?php echo $banner_sub->link?>">
                                <img style="height: 140px" alt="<?php echo $banner_sub->name?>" src="<?php echo $banner_sub->image_url?>">
                            </a>
                        </div>
                    </div>
                    <?php
                }
            }
        ?>


    </div>
</div>