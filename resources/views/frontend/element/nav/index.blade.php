<?php
$main_categories = \App\Models\ProductCategory::query()
    ->where('parent_id', 0)
    ->orderBy('order', 'asc')
    ->get();
?>

<header>


    <nav class="navbar navbar-inverse navbar-fixed-top top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button id="mobile-menu-btn" type="button" class="navbar-toggle collapsed pull-left">
                            <i class="fa fa-bars"></i>
                        </button>

                        <span class="hotline">
                            <span class="glyphicon glyphicon-earphone mrr5"></span> 0961.245.235
                        </span>

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar"
                                aria-expanded="false" aria-controls="navbar">
                            <i class="fa fa-angle-double-down"></i>
                        </button>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            <li><a href="/">Trang chủ</a></li>
                            <?php
                            if(isset($post_categories) && !empty($post_categories)) {
                            foreach($post_categories as $post_category) {
                            ?>
                            <li>
                                <a href="{!! \App\Models\Helper\PostCategory::getUrl($post_category->slug) !!}">
                                    <?php echo $post_category->title;?>
                                </a>
                            </li>
                            <?php
                            }
                            }
                            ?>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="middle">
        <div class="container">
            <div class="row">
                <div id="mobile-logo" class="container" style="margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{!! action('Frontend\Home@index') !!}">
                                <img class="logo" src="/frontend/img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <a href="{!! action('Frontend\Home@index') !!}">
                        <img id="logo" class="logo" src="/frontend/img/logo.png" alt="" style="margin-top: 15px;">
                    </a>
                </div>
                <div class="col-md-7 text-center">
                    <div class="tb full-width">
                        <div class="tr">
                            <div class="td">
                                <div>
                                    <img class="service-header-img" src="/frontend/img/icon-1.png" alt="">
                                </div>
                                <div class="c-red">
                                    HÀNG CHÍNH HÃNG
                                </div>
                            </div>
                            <div class="td">
                                <div>
                                    <img class="service-header-img" src="/frontend/img/icon-2.png" alt="">
                                </div>
                                <div class="c-red">
                                    MIỄN PHÍ VẬN CHUYỂN
                                </div>
                            </div>
                            <div class="td">
                                <div>
                                    <img class="service-header-img" src="/frontend/img/icon-3.png" alt="">
                                </div>
                                <div class="c-red">
                                    BẢO HÀNH UY TÍN
                                </div>
                            </div>
                            <div class="td">
                                <div>
                                    <img class="service-header-img" src="/frontend/img/icon-4.png" alt="">
                                </div>
                                <div class="c-red">
                                    TRẢ GÓP 0%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-right mobile-hide">
                    <div class="contact-phone-group">
                        <span class="cskh-label">TỔNG ĐÀI CSKH</span>
                        <span class="glyphicon glyphicon-earphone contact-phone"> 0961.245.235</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar bottom">
        <div class="container">
            <div class="row" style="position: relative">
                <div id="desktop-menu" class="col-md-8" style="position: static;">
                    <div class="menus row">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/">
                                    <i class="fa fa-home" style="font-size: 18px"></i> TRANG CHỦ
                                </a>
                            </li>
                            <?php
                            if(isset($main_categories) && !empty($main_categories)) {
                            foreach($main_categories as $main_category) {
                            ?>
                            <li>
                                <a href="<?php echo \App\Models\ProductCategory::getLink($main_category);?>">
                                    <?php echo $main_category->name;?>
                                </a>
                                <?php
                                $second_categories = \App\Models\ProductCategory::query()
                                    ->where('parent_id', $main_category->id)
                                    ->orderBy('order', 'asc')
                                    ->get();
                                ?>
                                <div class="dropdown-menu">
                                    <div class="col-md-12">
                                        <ul class="menu">
                                            <li>
                                                <ul class="sub-menu">
                                                    <?php if(isset($second_categories) && !empty($second_categories)){
                                                    foreach($second_categories as $second_category) {
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo \App\Models\ProductCategory::getLink($main_category, $second_category);?>"
                                                           title="<?php echo $second_category->name;?>">
                                                            <?php echo $second_category->name;?>
                                                        </a>
                                                    </li>
                                                    <?php
                                                    }
                                                    }?>


                                                </ul>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <?php
                            }
                            }
                            ?>


                        </ul>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <form action="/tim-kiem" method="get" class="search">
                        <input type="text" class="search-input" name="q" value="{{ request()->get('q','') }}"
                               placeholder="Bạn muốn tìm sản phẩm nào?">
                        <button class="btn btn-sm btn-danger" style="margin-top: 9px" type="submit">
                            <i class="fa fa-search"></i>
                            Tìm kiếm
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </nav>
</header>

<div id="mobile-menu" class="mobile-menu">
    <div class="row">
        <div class="col-md-12">
            <ul>
                <li><a href="/">TRANG CHỦ</a></li>

                @if(isset($main_categories) && !empty($main_categories))
                    @foreach($main_categories as $main_category)
                        <li>
                            <a href="{{\App\Models\ProductCategory::getLink($main_category)}}">
                                {{$main_category->name}}
                            </a>
                            <?php
                            $second_categories = \App\Models\ProductCategory::query()
                                ->where('parent_id', $main_category->id)
                                ->orderBy('order', 'asc')
                                ->get();
                            ?>

                            <i class="fa fa-angle-down {{isset($second_categories) && !empty($second_categories)?"_has_sub":''}}"></i>

                            <ul class="sub-menu">
                                @if(isset($second_categories) && !empty($second_categories))
                                    @foreach($second_categories as $second_category)
                                        <li>
                                            <a href="{{\App\Models\ProductCategory::getLink($main_category, $second_category)}}">
                                                <i class="fa fa-angle-right"></i> {{$second_category->name}}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>

