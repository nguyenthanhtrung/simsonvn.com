@if(isset($breadcrumb) && is_array($breadcrumb) && !empty($breadcrumb))
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{isset($breadcrumb['title'])?$breadcrumb['title']:''}}</h2>
            @if(isset($breadcrumb['data']))
                <ol class="breadcrumb">
                    @foreach($breadcrumb['data'] as $item)
                        <li class="{{isset($item['active']) && $item['active']?'active':''}}">
                            <a href="{{isset($item['url'])?$item['url']:'#'}}">{{isset($item['name'])?$item['name']:''}}</a>
                        </li>
                    @endforeach
                </ol>
            @endif
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endif