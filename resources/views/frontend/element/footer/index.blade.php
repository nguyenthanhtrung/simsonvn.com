<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <img class="phone-icon" src="/frontend/img/icon-phone.png"/>

                <b>TỔNG ĐÀI ĐÓNG GÓP KHIẾU NẠI</b>
                (09h00 - 21h00 tất cả các ngày trong tuần)
                <span class="c-red"><b>0961.245.235 - 01684.362.369</b></span>
            </div>
        </div>
    </div>

    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3>HỖ TRỢ KỸ THUẬT</h3>

                    <p class="phone">0961.245.235 - 01684.362.369</p>

                    <h3>CHÍNH SÁCH & QUY ĐỊNH CHUNG</h3>

                    <ul>
                        <?php
                        $posts = \App\Models\Post::query()->where('category_id', 3)->get();
                        if(isset($posts) && !empty($posts)) {
                        foreach($posts as $post) {
                        ?>
                        <li>
                            <a href="<?php echo \App\Models\Helper\Post::getUrl($post->slug, $post->id);?>">
                                <?php echo $post->title;?>
                            </a>
                        </li>
                        <?php
                        }
                        }
                        ?>


                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>CHĂM SÓC KHÁCH HÀNG</h3>

                    <p><b>Hệ thống bán lẻ di động Tây Bắc Mobile</b></p>
                    <p>Cơ sở 1: 369 đường Trần Hưng Đạo. Tp Lai Châu.</p>
                    <p>Cơ sở 2: 161 Thái Hà. Hà Nội.</p>
                    <p>Cơ sở 3: 544 đường Điện Biên. phường Duyên Hải, Tp Lào Cai.</p>

                    <p><b>Email : taybacmobile@gmail.com</b></p>
                    <p><b>Tổng đài : 0961.245.235 - 01684.362.369</b></p>
                </div>
                <div class="col-md-4">
                    <h3>FACEBOOK</h3>

                    <div class="fb-page" data-href="https://www.facebook.com/taybacmobile.vn/" data-tabs="timeline"
                         data-height="250px" data-small-header="false" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/taybacmobile.vn/" class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/taybacmobile.vn/">Hệ thống bán lẻ di động Tây Bắc
                                mobile</a>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p>Hệ thống bán lẻ di động Tây Bắc mobile - 369 đường Trần Hưng Đạo. Tp Lai Châu.</p>
                <p>Điện thoại: 0961.245.235 - 01684.362.369 &nbsp;&nbsp;Email: taybacmobile@gmail.com.</p>
            </div>
        </div>
    </div>
</footer>

<div id="back-to-top" class="to-top">
    <a href="#top"><i class="fa fa-level-up"></i></a>
</div>
<a href="tel:0961245235" class="to-phone"><i class="fa fa-phone"></i></a>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/frontend/js/bootstrap.js"></script>
<script src="/frontend/js/xzoom.js"></script>
<script src="/frontend/js/app.js"></script>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=886035664907863";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
            d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set._.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute("charset", "utf-8");
        $.src = "https://v2.zopim.com/?4wrpooJWG22nDlmcd7Fz1oiXxgYL0ySq";
        z.t = +new Date;
        $.type = "text/javascript";
        e.parentNode.insertBefore($, e)
    })(document, "script");
</script>
<script>
    $(".xzoom").xzoom({tint: '#333', Xoffset: 0});

    $(document).on('click', '#mobile-menu-btn', function () {
        var main_container = $('#main-container');

        if (main_container.hasClass('collapsed')) {
            main_container.removeClass('collapsed');
        } else {
            main_container.addClass('collapsed');

            $('#mobile-menu').css({height: window.innerHeight + 'px'});
        }
    });

    $(document).on('click', '#main-container.collapsed', function (e) {
        var main_container = $('#main-container'), target = e.target;

        if (main_container.hasClass('collapsed') && !$(target).closest('#mobile-menu').hasClass('mobile-menu')) {
            main_container.removeClass('collapsed');
        }
    });

    $(document).on('click', '#mobile-menu ul > li > ._has_sub', function () {
        var $this = $(this), li = $this.parent('li');

        if (li.hasClass('collapsed')) {
            li.removeClass('collapsed');
            $this.removeClass('fa-angle-up').addClass('fa-angle-down');
            li.find('.sub-menu').hide();
        } else {
            li.addClass('collapsed');
            $this.removeClass('fa-angle-down').addClass('fa-angle-up');
            li.find('.sub-menu').show();
        }
    });

    $(document).on('click', '.products-header-categories-c > .fa.collapse', function (e) {
        var $this = $(this), parent = $this.parent('.products-header-categories-c');
        console.log('fdf');
        parent.find('.products-header-categories.pull-right').toggle();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.search:before').click(function() {
            alert('fuck');
        });
    });
</script>
@section('script.footer')

@show
<!--End of Zendesk Chat Script-->