@extends('frontend.layout.product')

@section('title', "Tây Bắc Mobile")

@section('content')




    <div class="products-header category">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h1>
                            <span>Kết quả tìm kiếm</span>
                        </h1>

                        <p class="subtitle">Tìm thấy {!! $products->total() !!} kết quả phù hợp từ khóa "{{ request()->get('q') }}"</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-filter">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="filter">
                        <li class="first-li">
                            <i class="fa fa-search"></i> &nbsp;
                            DANH SÁCH KẾT QUẢ</li>

                        <li class="last-li"><a href="javascript:;">Sắp xếp theo giá<span class="arrdown"></span></a>
                            <ul class="dropfilter" style="width: 160px;">
                                <li>
                                    <a href="/dien-thoai-xiaomi/?&amp;hdh=android&amp;&amp;&amp;sx=gia-thap-den-cao">Giá
                                        thấp đến
                                        cao</a>
                                    <a href="/dien-thoai-xiaomi/?&amp;hdh=android&amp;&amp;&amp;sx=gia-cao-den-thap">Giá
                                        cao đến
                                        thấp</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="products-body">
        <div class="container">
            <div class="row">
                @if(isset($products) && !empty($products))
                    @include('frontend.product.lists',['products'=>$products])

                @endif


            </div>
            <div class="row">
                {!! $products->render() !!}
            </div>
        </div>

        <!--<div class="container">

    </div>

@stop