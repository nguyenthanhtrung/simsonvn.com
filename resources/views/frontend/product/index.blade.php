@extends('frontend.layout.default')

@section('title', "Simsonvn")

@section('content')

    <div id="heading-breadcrumbs">
        <div class="container">
            <div class="row d-flex align-items-center flex-wrap">
                <div class="col-md-7">
                    <h1 class="h2">
                        {{!empty($sub_category)?$sub_category->slug:$main_category->name}}
                    </h1>
                </div>
                <div class="col-md-5">
                    <ul class="breadcrumb d-flex justify-content-end">
                        <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>

                        @if(!empty($sub_category))
                            <li class="breadcrumb-item">
                                <a href="{!! action('Frontend\Product@index',[$main_category->slug]) !!}">
                                    {{$main_category->name}}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">{{$sub_category->slug}}</li>
                        @else
                            <li class="breadcrumb-item active">{{$main_category->name}}</li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    @if(!$main_product_categories->isEmpty())
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Danh mục</h3>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked category-menu">
                                    @foreach($main_product_categories as $main_product_category)
                                        <li>
                                            <a href="{{\App\Models\ProductCategory::getLink($main_product_category)}}">{{$main_product_category->name}}</a>
                                            @if(!$main_product_category->categories->isEmpty())
                                                <ul>
                                                    @foreach($main_product_category->categories as $second_category)
                                                        <li>
                                                            <a href="{{\App\Models\ProductCategory::getLink($main_product_category, $second_category)}}">
                                                                {{$second_category->name}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-sm-9">
                    @if(!$products->isEmpty())
                        @include('frontend.product.lists',['products'=>$products])

                        {!! $products->render() !!}
                    @else
                        <p>Không tìm thấy sản phẩm theo yêu cầu</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop