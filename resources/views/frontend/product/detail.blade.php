@extends('frontend.layout.default')

@section('title', $product->name)

@section('content')

    <div id="heading-breadcrumbs">
        <div class="container">
            <div class="row d-flex align-items-center flex-wrap">
                <div class="col-md-7">
                    <h1 class="h2">{{$product->name}}</h1>
                </div>
                <div class="col-md-5">
                    <ul class="breadcrumb d-flex justify-content-end">
                        <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>

                        @if(isset($first_pr_category))
                            <li class="breadcrumb-item">
                                <a href="{!! \App\Models\ProductCategory::getLink($first_pr_category) !!}">
                                    {{$first_pr_category->name}}
                                </a>
                            </li>
                        @endif

                        @if(isset($first_pr_category) && isset($second_pr_category))
                            <li class="breadcrumb-item">
                                <a href="{!! \App\Models\ProductCategory::getLink($first_pr_category, $second_pr_category) !!}">
                                    {{$second_pr_category->name}}
                                </a>
                            </li>
                        @endif

                        <li class="breadcrumb-item active">{{$product->name}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">

            <div class="row">
                <div class="col-sm-3">
                    @if(!$main_product_categories->isEmpty())
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Danh mục</h3>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked category-menu">
                                    @foreach($main_product_categories as $main_product_category)
                                        <li>
                                            <a href="{{\App\Models\ProductCategory::getLink($main_product_category)}}">{{$main_product_category->name}}</a>
                                            @if(!$main_product_category->categories->isEmpty())
                                                <ul>
                                                    @foreach($main_product_category->categories as $second_category)
                                                        <li>
                                                            <a href="{{\App\Models\ProductCategory::getLink($main_product_category, $second_category)}}">
                                                                {{$second_category->name}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-9">
                    <div class="row" id="productMain">
                        <div class="col-sm-6">
                            <div id="mainImage">
                                <img src="{{$product->image}}" alt="{{$product->name}}" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box">

                                <form>
                                    <div class="sizes">
                                        <h3>{{$product->name}}</h3>
                                    </div>

                                    {{--<p class="price">{!!  \App\Common\Utility::numberFormat($product->price) !!}VNĐ</p>--}}
                                    <p class="price">GIÁ LIÊN HỆ</p>

                                    {{--<p class="text-center">
                                        <button type="submit" class="btn btn-template-main"><i
                                                    class="fa fa-shopping-cart"></i> Mua ngay
                                        </button>
                                    </p>--}}

                                </form>
                            </div>

                            {{--@if(!$product_images->isEmpty())
                                <div class="row" id="thumbs">
                                    <div class="col-xs-4">
                                        <a href="{{$product->image}}" class="thumb">
                                            <img src="{{$product->image}}" alt=""
                                                 class="img-responsive">
                                        </a>
                                    </div>
                                    @foreach($product_images as $product_image)
                                        <div class="col-xs-4">
                                            <a href="{{$product_image->url}}" class="thumb">
                                                <img src="{{$product_image->url}}" alt=""
                                                     class="img-responsive">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif--}}
                        </div>

                    </div>


                    <div class="box" id="details">
                        {!! $product->description !!}
                    </div>

                    @if(!$product_relations->isEmpty())
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="box text-uppercase">
                                    <h3>Sản phẩm liên quan</h3>
                                </div>
                            </div>

                            @foreach($product_relations as $product_same_price)
                                <?php $link = \App\Models\Product::getLink($product_same_price); ?>

                                <div class="col-md-3 col-sm-6">
                                    <div class="product">
                                        <div class="image">
                                            <a href="{{$link}}">
                                                <img src="{{$product_same_price->image}}"
                                                     alt="{{$product_same_price->name}}"
                                                     class="img-responsive image1">
                                            </a>
                                        </div>
                                        <div class="text">
                                            <h3>{{$product_same_price->name}}</h3>
                                            <p class="price">
                                                {{\App\Common\Utility::numberFormat($product_same_price->price)}}VNĐ
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>


            </div>

        </div>
    </div>

@stop

@section('script.footer')
    @parent

@endsection