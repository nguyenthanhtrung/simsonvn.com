@if(!$products->isEmpty())
    <div class="row products">

        @foreach ($products as $product)
            <?php
            $compare_price = $product->price - $product->price_promotion;
            $compare_price_percent = round($compare_price / ($product->price / 100), 1);
            $link = \App\Models\Product::getLink($product);
            ?>

            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <div class="image" style="height: 188px;border: 1px solid #CDF;">
                        <a href="{{$link}}">
                            <img style="height: 100%;" src="{{$product->image}}" alt="" class="img-responsive image1">
                        </a>
                    </div>
                    <div class="text">
                        <h3><a href="{{$link}}">{{$product->name}}</a></h3>
                        <p class="price" style="padding-top: 10px">
                            Giá liên hệ
                            {{--{{\App\Common\Utility::numberFormat($product->price_promotion)}} VNĐ--}}
                        </p>
                    </div>

                    @if($compare_price_percent > 0)
                        <div class="ribbon sale">
                            <div class="theribbon">{{$compare_price_percent}}%</div>
                            <div class="ribbon-background"></div>
                        </div>
                    @endif

                    <div class="ribbon new">
                        <div class="theribbon">MỚI</div>
                        <div class="ribbon-background"></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif