@extends('frontend.layout.default')

@section('title', $post->title)

@section('content')

    <div id="heading-breadcrumbs">
        <div class="container">
            <div class="row d-flex align-items-center flex-wrap">
                <div class="col-md-7">
                    <h1 class="h2">
                        {{$post->title}}
                    </h1>
                </div>
                <div class="col-md-5">
                    <ul class="breadcrumb d-flex justify-content-end">
                        <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                        @if(!empty($post_category))
                            <li class="breadcrumb-item">
                                <a href="{!! \App\Models\Helper\PostCategory::getUrl($post_category->slug) !!}">
                                    {{$post_category->title}}
                                </a>
                            </li>
                        @endif
                        <li class="breadcrumb-item active">{{$post->title}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">

            <div class="row">
                <div class="col-md-12" id="blog-post">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
    </div>

@stop