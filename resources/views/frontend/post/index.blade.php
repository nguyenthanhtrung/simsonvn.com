@extends('frontend.layout.default')

@section('title', $post_category->title)

@section('content')

    <div id="heading-breadcrumbs">
        <div class="container">
            <div class="row d-flex align-items-center flex-wrap">
                <div class="col-md-7">
                    <h1 class="h2">
                        {{$post_category->title}}
                    </h1>
                </div>
                <div class="col-md-5">
                    <ul class="breadcrumb d-flex justify-content-end">
                        <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                        <li class="breadcrumb-item active">{{$post_category->title}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="blog-listing-medium">
                    @if(!empty($posts))
                        @foreach($posts as $post)
                            <?php $post_link = \App\Models\Helper\Post::getUrl($post->slug, $post->id); ?>

                            <section class="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="image" style="height: 208px;">
                                            <a href="{{$post_link}}">
                                                <div class="card-box">
                                                    <div class="card-box--content">
                                                        <img src="{{$post->image}}" class="img-responsive"
                                                             alt="{{$post->title}}">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h2>
                                            <a href="{{\App\Models\Helper\Post::getUrl($post->slug, $post->id)}}">{{$post->title}}</a>
                                        </h2>
                                        <p class="intro">{{$post->introduce}}</p>
                                    </div>
                                </div>
                            </section>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop