@extends('frontend.layout.default')

@section('title', 'Trang chủ')

@section('content')
    @if(!empty($banners))
        <div class="homepage owl-carousel" style="height: 501px;">
            @foreach ($banners as $banner)
                <div class="item" style="height: 501px;">
                    <img src="{{$banner->image_url}}" alt="{{$banner->name}}"/>
                </div>
            @endforeach
        </div>
    @endif

    <div id="content" style="margin-top: 20px;">
        <div class="container">


            <div class="row products">
                <div class="col-md-12">
                    <div class="heading">
                        <h2>Sản phẩm nổi bật</h2>
                    </div>
                </div>
                <?php
                if(isset($product_categories) && !empty($product_categories)) {
                    foreach($product_categories as $product_category) {
                    ?>
                    <div class="col-md-3 col-sm-4">
                        <div class="product">
                            <div class="image" style="height: 263px;width: 263px;">
                                <a href="{{\App\Models\ProductCategory::getLink($product_category)}}">
                                    <img style="width: 100%;height: 100%" src="{!! $product_category->image_url !!}" alt=""
                                         class="img-responsive image1">
                                </a>
                            </div>
                            <div class="text">
                                <h3>
                                    <a href="{{\App\Models\ProductCategory::getLink($product_category)}}">{!! $product_category->name !!}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                }
                ?>


            </div>
            <div class="row" style="margin-bottom: 20px;">

                <div class="col-md-12">

                </div>

                <div class="col-md-6" id="blog-listing-medium">
                    <div class="heading">
                        <h2>Tin tức nổi bật</h2>
                    </div>
                    @if(!$posts->isEmpty())

                        <ul class="posts">
                            @foreach($posts as $post)
                                <?php $post_link = \App\Models\Helper\Post::getUrl($post->slug, $post->id); ?>
                                <li><a href="{!! $post_link !!}">» {{$post->title}}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>

                <div class="col-md-6">
                    <div class="heading">
                        <h2>Video quảng cáo</h2>
                    </div>
                    @if($video instanceof \App\Models\Video)
                        <div class="video">
                            <div class="embed-responsive embed-responsive-16by9">
                                {!! $video->embed_code !!}
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>

    @if(!$partners->isEmpty())
        <section class="bar background-gray no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h2>Đối tác</h2>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-left: 0">

                        <ul class="owl-carousel customers no-mb">
                            @foreach($partners as $partner)
                                <li class="item">
                                    <img src="{{$partner->image}}" alt="{{$partner->name}}" class="img-responsive">
                                </li>
                            @endforeach
                        </ul>
                        <!-- /.owl-carousel -->
                    </div>

                </div>
            </div>
        </section>
    @endif
@stop