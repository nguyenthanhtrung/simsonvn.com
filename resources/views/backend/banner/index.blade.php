@extends('backend.layout.default')

@section('title', "Danh sách Banner")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('banner.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>

                        @if($banners)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th class="col-sm-1 text-center">Banner</th>
                                    <th>Tên</th>
                                    <th class="col-sm-1 text-center">Sắp xếp</th>
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-2 text-center">Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td class="text-center">
                                            <img src="{!! $banner->image_url !!}" alt="{!! $banner->name !!}"
                                                 style="max-height: 100px; max-width: 100px;">
                                        </td>
                                        <td>
                                            <strong>
                                                <a href="{!! route('banner.edit',['id'=>$banner->id]) !!}"
                                                   title="Sửa">
                                                 {{$banner->name}}
                                                </a>
                                            </strong>
                                        </td>
                                        <td class="text-center">{{$banner->order}}</td>
                                        <td class="text-center">{{\App\Common\Utility::displayDatetime($banner->created_time)}}</td>
                                        <td class="text-center">


                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('banner.edit',['id'=>$banner->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! action('Backend\Banner@delete',['id'=>$banner->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $banners->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop