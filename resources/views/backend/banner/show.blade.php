@extends('backend.layout.default')

@section('title', $banner->name)

@section('content')

    <div class="wrapper wrapper-content">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="150px">#</th>
                            <th>Giá trị</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ID</td>
                            <td>{!! $banner->id !!}</td>
                        </tr>
                        <tr>
                            <td>Tên</td>
                            <td>{!! $banner->name !!}</td>
                        </tr>
                        <tr>
                            <td>Hình ảnh</td>
                            <td><img src="{!! $banner->image_url !!}" alt="" style="height: 100px;"></td>
                        </tr>
                        <tr>
                            <td>Mô tả</td>
                            <td>{!! $banner->description !!}</td>
                        </tr>
                        <tr>
                            <td>Sắp xếp</td>
                            <td>{!! $banner->order !!}</td>
                        </tr>
                        <tr>
                            <td>Thời gian cập nhật</td>
                            <td>{!! $banner->updated_time !!}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop