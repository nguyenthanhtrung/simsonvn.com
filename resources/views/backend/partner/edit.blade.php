@extends('backend.layout.default')

@section('title', $data->name)

@section('style.header')
    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="/backend/partner/{{$data->id}}" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">

                {{csrf_field()}}
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Đối tác</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name"
                                               placeholder="Tên Đối tác"
                                               value="{{$data->name}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="introduce" class="col-sm-2 control-label">Mô tả</label>

                                    <div class="col-sm-10">
                                                    <textarea name="introduce" id="content"
                                                              class="form-control">{{$data->introduce}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="content" class="col-sm-2 control-label">Url</label>

                                    <div class="col-sm-10">
                                                    <textarea name="url" id="url"
                                                              class="form-control summernote">{{$data->url}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">Trạng thái</label>

                                    <div class="col-sm-10">
                                        <input type="checkbox" name="status"
                                               id="status" {{$data->status?'checked':''}}>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>


                                <div class="form-group">
                                    <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="order"
                                               name="order" placeholder="Sắp xếp"
                                               value="{{$data->order}}">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ảnh đại diện</label>

                                    <div class="col-sm-10">
                                        <input type="file" name="thumb" class="form-control"
                                               accept="image/*"/>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <button class="btn btn-primary" type="submit"><i
                                                    class="fa fa-check"></i> Cập nhật
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@section('script.footer')

    @parent
@endsection
