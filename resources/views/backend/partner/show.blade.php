@extends('backend.layout.default')

@section('title', $post->name)

@section('content')

    <div class="wrapper wrapper-content">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="150px">#</th>
                            <th>Giá trị</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ID</td>
                            <td>{!! $post->id !!}</td>
                        </tr>
                        <tr>
                            <td>Bài viết</td>
                            <td><a href="{!! \App\Models\Helper\Post::getUrl($post->id, $post->title) !!}"
                                   target="_blank">{!! $post->title !!}</a></td>
                        </tr>
                        <tr>
                            <td>Mô tả</td>
                            <td>{!! $post->description !!}</td>
                        </tr>
                        <tr>
                            <td>Nội dung</td>
                            <td>{!! $post->content !!}</td>
                        </tr>
                        <tr>
                            <td>Tạo bởi</td>
                            <td>{{$post->created_by_name}} vào
                                lúc {!! \App\Commons\Utility::makeFriendlyTime($post->created_at) !!}</td>
                        </tr>
                        <tr>
                            <td>Cập nhật bởi</td>
                            <td>{{$post->updated_by_name}} vào
                                lúc {!! \App\Commons\Utility::makeFriendlyTime($post->updated_at) !!}</td>
                        </tr>
                        <tr>
                            <td>Trạng thái</td>
                            <td>
                                @if($post->active == 1)
                                    <span class="text-success">Xuất bản</span>
                                @else
                                    <span class="text-warning">Chưa xuất bản</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop