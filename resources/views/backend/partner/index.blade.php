@extends('backend.layout.default')

@section('title', "Danh sách đối tác")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('partner.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>

                        @if($data)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="50" class="text-center">ID</th>
                                    <th class="col-sm-1">Ảnh đại diện</th>
                                    <th>Đối tác</th>
                                    <th class="col-sm-1 text-center">Trạng thái</th>
                                    <th class="col-sm-1 text-center">Sắp xếp</th>
                                    <th width="120" class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($data as $item)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td>
                                            <img src="{!! $item->image !!}" alt="{!! $item->name !!}"
                                                 style="max-height: 100px; max-width: 100px;">
                                        </td>
                                        <td>
                                            <a href="{!! route('post.edit',['id'=>$item->id]) !!}"
                                               target="_blank">{!! $item->name !!}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if($item->status == 1)
                                                <span class="text-success">Hoạt động</span>
                                            @else
                                                <span class="text-gray">Không hoạt động</span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            {!! $item->order !!}
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('partner.edit',['id'=>$item->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('partner.destroy',['id'=>$item->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $data->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
