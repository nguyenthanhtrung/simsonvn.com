@extends('backend.layout.default')

@section('title', "Danh sách Banner")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('video.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>

                        @if($datas)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 60px" class="text-center">ID</th>
                                    <th>Thông tin chung</th>
                                    <th class="col-sm-1 text-center">Sắp xếp</th>
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-1 text-center">Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($datas as $tmp)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>

                                        <td>
                                            <strong>
                                                <a href="{!! route('video.edit',['id'=>$tmp->id]) !!}">
                                                    {{$tmp->name}}
                                                </a>
                                            </strong>
                                        </td>
                                        <td class="text-center">{{$tmp->order}}</td>
                                        <td class="text-center">
                                            {{ \App\Common\Utility::displayDatetime($tmp->created_time) }}
                                        </td>
                                        <td class="text-center">


                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('video.edit',['id'=>$tmp->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 delete_item"
                                               href="{!! url('backend/video/delete',['id'=>$tmp->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $datas->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop