@extends('backend.layout.default')

@section('title', "Thêm video")

@section('style.header')

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">

            <form method="POST" action="/backend/video" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST" enctype="multipart/form-data">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Tên video</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="name" name="name"
                                                        placeholder="Tên video"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="embed_code" class="col-sm-2 control-label">Mã nhúng</label>

                                            <div class="col-sm-10">
                                            <textarea name="embed_code" id="description"
                                                      class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="order"
                                                       name="order" placeholder="Sắp xếp"
                                                       value="0">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="order" class="col-sm-2 control-label">Vị trí</label>

                                            <div class="col-sm-10">
                                                <select name="position" class="form-control">
                                                    <option value="">Tất cả</option>
                                                    <option value="banner_side">Cạnh banner</option>
                                                    <option value="banner_bottom">Dưới banner</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

@stop

@section('script.footer')

    @parent
@endsection
