<!DOCTYPE html>
<html>

<head>

    @include('backend.element.header.index')

</head>

<body>

<div id="wrapper">

    @include('backend.element.nav.index')

    <div id="page-wrapper" class="gray-bg dashbard-1">

        @include('backend.element.nav.top')

        @include('backend.element.breadcrumb')

        @yield('content')

    </div>
</div>

@include('backend.element.footer.index')

</body>
</html>
