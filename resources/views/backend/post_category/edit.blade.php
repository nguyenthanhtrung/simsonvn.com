@extends('backend.layout.default')

@section('title', $post_category->name)

@section('style.header')
    @parent

    <link href="/themes/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/themes/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="/backend/post_category/{{$post_category->id}}" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Bài viết</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="title" class="col-sm-2 control-label">Danh mục</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="title" name="title"
                                                       class="form-control" placeholder="Tên Bài viết"
                                                       value="{{$post_category->title}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="title" class="col-sm-2 control-label">Danh mục cha</label>

                                            <div class="col-sm-10">
                                                <select class="form-control m-b" name="parent_id">
                                                    <option value="0">Chọn Danh mục cha</option>
                                                    @foreach($post_categories as $key=>$value)
                                                        <option value="{{$key}}" {{$post_category->parent_id == $key?'selected':''}}>
                                                            {{$value}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!--<div class="form-group">
                                            <label for="title" class="col-sm-2 control-label">Sắp xếp</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="order" name="order"
                                                       class="form-control" placeholder="Sắp xếp"
                                                       value="{{$post_category->order}}">
                                            </div>
                                        </div>-->

                                        <div class="form-group">
                                            <label for="status" class="col-sm-2 control-label">Trạng thái</label>

                                            <div class="col-sm-10">
                                                <input type="checkbox" name="status"
                                                       id="status" {{$post_category->status?'checked':''}}>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>

@stop

@section('script.footer')

    @parent
@endsection
