@extends('backend.layout.default')

@section('title', "Thêm Danh mục")

@section('style.header')
    @parent

    <link href="/themes/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/themes/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-8">

            <form method="POST" action="/backend/post_category" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">

                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="title" class="col-sm-2 control-label">Danh mục</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="title" name="title"
                                                       placeholder="Tên Bài viết"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="title" class="col-sm-2 control-label">Danh mục cha</label>

                                            <div class="col-sm-10">
                                                <select class="form-control m-b" name="parent_id">
                                                    <option value="0">Chọn Danh mục cha</option>
                                                    @foreach($post_categories as $key=>$value)
                                                        <option value="{{$key}}">
                                                            {{$value}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="order" name="order"
                                                        placeholder="Sắp xếp" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="status" class="col-sm-2 control-label">Trạng
                                                thái</label>

                                            <div class="col-sm-10">
                                                <input type="checkbox" name="status"
                                                       id="status">
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

@stop

@section('script.footer')
    <script src="/themes/inspinia/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);
                data.append("type", '{{\App\Common\UploadedImage::POST_EDITOR}}');

                $.ajax({
                    type: "POST",
                    url: "/backend/image/editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>

    @parent
@endsection
