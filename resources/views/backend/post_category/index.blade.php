@extends('backend.layout.default')

@section('title', "Danh mục Bài viết")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('post_category.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>

                        @if($post_categories)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th>Danh mục</th>
                                    <th class="col-sm-2 text-center">Slug</th>
                                    <th class="col-sm-1 text-center">Thể loại cha</th>
                                    <th class="col-sm-1 text-center">Trạng thái</th>
                                    <th class="col-sm-2 text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($post_categories as $post_category)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td>
                                            <strong>
                                                <a href="{!! route('post_category.edit',['id'=>$post_category->id]) !!}" target="_blank">
                                                    {!! $post_category->title !!}
                                                </a>
                                            </strong>
                                        </td>
                                        <td>{!! $post_category->slug !!}</td>
                                        <td class="text-center">{!! $post_category->parent_id !!}</td>
                                        <td class="text-center">
                                            @if($post_category->status == 1)
                                                <span class="text-success">ACTIVE</span>
                                            @else
                                                <span class="text-warning">INACTIVE</span>
                                            @endif
                                        </td>
                                        <td width="150">


                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('post_category.edit',['id'=>$post_category->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('post_category.destroy',['id'=>$post_category->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $post_categories->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop