@extends('backend.layout.default')

@section('title', $post_category->title)

@section('content')

    <div class="wrapper wrapper-content">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="150px">#</th>
                            <th>Giá trị</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ID</td>
                            <td>{!! $post_category->id !!}</td>
                        </tr>
                        <tr>
                            <td>Bài viết</td>
                            <td>{!! $post_category->title !!}</td>
                        </tr>
                        <tr>
                            <td>Thể loại cha</td>
                            <td>{!! $post_category->parent_id !!}</td>
                        </tr>
                        <tr>
                            <td>Sắp xếp</td>
                            <td>{!! $post_category->order !!}</td>
                        </tr>
                        <tr>
                            <td>Trạng thái</td>
                            <td>
                                @if($post_category->active == 1)
                                    <span class="text-success">ACTIVE</span>
                                @else
                                    <span class="text-warning">INACTIVE</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop