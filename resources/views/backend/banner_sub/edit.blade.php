@extends('backend.layout.default')

@section('title', $odm->name)

@section('style.header')

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-8">
            <form method="POST" action="/backend/banner_sub/{{$odm->id}}" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        @include('backend.element.alert',[])
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Tên video</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="name" name="name"
                                                       placeholder="Link"
                                                       value="{!! $odm->name !!}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="link"
                                                       placeholder="Link"
                                                       value="{!! $odm->link !!}">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="order"
                                                       name="order" placeholder="Sắp xếp"
                                                       value="{!! $odm->order !!}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">File ảnh</label>

                                            <div class="col-sm-10">
                                                <?php
                                                    if(isset($odm) && !empty($odm->image_url)) {
                                                        ?>
                                                        <img src="<?php echo $odm->image_url;?>"
                                                             style="width: 160px;height: 70px;border:1px solid #ccc" />
                                                        <br/>
                                                        <br/>
                                                        <?php
                                                    }
                                                ?>

                                                <input type="file" name="image" class="form-control"
                                                       accept="image/*"/>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>

@stop
