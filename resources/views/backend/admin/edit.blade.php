@extends('backend.layout.default')

@section('title', $user->username)



@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="/backend/user/{{$user->id}}" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Người dùng</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="username" class="col-sm-2 control-label">Username</label>

                                            <div class="col-sm-10">
                                                <input disabled="disabled" type="text" class="form-control" id="username" name="username"
                                                       class="form-control"
                                                       value="{{$user->username}}" disabled>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="status" class="col-sm-2 control-label">Trạng thái</label>

                                            <div class="col-sm-10">
                                                <input type="checkbox" name="status"
                                                       id="status" {{$user->status?'checked':''}}>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>

@stop
