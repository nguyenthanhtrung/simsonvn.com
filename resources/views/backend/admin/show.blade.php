@extends('backend.layout.default')

@section('title', $user->username)

@section('content')

    <div class="wrapper wrapper-content">
        <div class="input-group">
            <a class="btn btn-primary btn-sm" href="{!! route('admin.index') !!}">
                <i class="fa fa-database"></i>
                Danh sách
            </a>

        </div>
        <br/>
        <div class="col-lg-12 no-padding">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <table class="table table-bordered">

                        <tbody>

                        <tr>
                            <td class="col-sm-2">
                                <label>Tên người dùng</label>
                            </td>
                            <td>{!! $user->username !!}</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Tên đầy đủ</label>
                            </td>
                            <td>{!! $user->fullname !!}</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                            </td>
                            <td>{!! $user->email !!}</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Phone</label>
                            </td>
                            <td>{!! $user->phone !!}</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Số dư</label>
                            </td>
                            <td>0 VNĐ</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Trạng thái</label>
                            </td>
                            <td>
                                @if($user->active == 1)
                                    <span class="text-success">ACTIVE</span>
                                @else
                                    <span class="text-warning">INACTIVE</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Ngày tham gia</label>
                            </td>
                            <td>
                                {!! \App\Common\Utility::displayDatetime($user->created_time) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>TK trên hệ thống thanh toán</label>
                            </td>
                            <td>
                                {!! $user->ps_account_name !!} ({!! $user->ps_account_id !!})
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop

