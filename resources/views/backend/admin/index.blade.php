@extends('backend.layout.default')

@section('title', "Danh sách Người dùng")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('admin.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm quản trị</a>
                            </div>
                        </div>

                        @if($users)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th>Username</th>
                                    <th class="col-sm-2">Tên hiển thị</th>
                                    <th class="col-sm-2">Email</th>
                                    <th class="col-sm-1">Trạng thái</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0;?>
                                @foreach($users as $user)
                                    <?php $i++;?>
                                    <tr>
                                        <td class="text-center">{!! $i !!}</td>
                                        <td>
                                            <strong>
                                                <a href="{!! route('admin.show',['id'=>$user->id]) !!}">
                                                    {!! $user->username !!}
                                                </a>
                                            </strong>
                                        </td>
                                        <td>{!! $user->fullname !!}</td>
                                        <td>{!! $user->email !!}</td>
                                        <td>
                                            @if($user->status == 1)
                                                <span class="text-success">ACTIVE</span>
                                            @else
                                                <span class="text-warning">INACTIVE</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-white btn-xs m-l-3"
                                               href="{!! route('admin.show',['id'=>$user->id]) !!}"
                                               title="Xem">
                                                <i class="fa fa-search"></i> Xem
                                            </a>

                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('admin.edit',['id'=>$user->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('admin.destroy',['id'=>$user->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $users->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop