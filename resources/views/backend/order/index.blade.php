@extends('backend.layout.default')

@section('title', "Danh sách đơn hàng")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    @include('backend.element.alert',[])
                    <div class="ibox-content">
                        @if($orders)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 60px">ID</th>
                                    <th class="col-sm-1">Ảnh</th>
                                    <th>Sản phẩm</th>
                                    <th class="col-sm-3">Thông tin đặt hàng</th>
                                    <th class="col-sm-2 text-center">Trạng thái</th>
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-1 text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($orders as $order)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td>
                                            <img src="{!! $order->product_image !!}"
                                                 style="max-height: 100px; max-width: 100px;">
                                        </td>
                                        <td>
                                            {!! $order->product_name !!}
                                        </td>
                                        <td>
                                            {!! $order->name !!}
                                            <p>{!! $order->phone !!}</p>
                                            <p>{!! $order->address !!}</p>
                                            <p>{!! $order->description !!}</p>
                                        </td>
                                        <td class="text-center">
                                            {!! $order->displayTitle() !!}
                                            @if($order->status == 'PENDING')
                                                <br/>
                                                <br/>
                                                <a data-id="{!! $order->id !!}" data-plan="DONE" class="btn btn-xs btn-primary processed">Đã xử lý đơn hàng</a>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            {!! \App\Common\Utility::displayDatetime($order->created_time) !!}
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-danger btn-xs m-l-3 _item_delete"
                                               href="{!! action('Backend\Order@delete',['id'=>$order->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $orders->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script.footer')
    @parent

    <script type="text/javascript">


        $(document).ready(function(){
            $('._item_delete').click(function(event) {
                event.preventDefault();
                if(!confirm('Bạn có thực sự muốn xóa?')) {
                    return false;
                }

                location.href = $(this).attr('href');
            });
            $('.processed').click(function() {
                if(!confirm('Bạn có chắc muốn xử lý đơn hàng này ?')) {
                    return false;
                }
                var order_id = $(this).attr('data-id');
                var plan = $(this).attr('data-plan');
                $.ajax({
                    url:'/backend/order/change_status',
                    data:{
                        id:order_id,
                        status:plan
                    },
                    type:'post',
                    success:function() {
                        alert('Đã xử lý đơn hàng');
                        location.reload(true);
                    }
                });
            });
        });
    </script>
@endsection