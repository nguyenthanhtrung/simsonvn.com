@extends('backend.layout.login')

@section('title', "Đăng nhập")

@section('content')

    <div>
        <h3>Trang quản trị</h3>


        <form class="m-t" role="form" action="{!! action('Backend\Login@login') !!}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <input type="text" name="username" class="form-control" required=""
                       autocomplete="off">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" required=""
                       autocomplete="off">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Đăng nhập</button>
        </form>
    </div>

@stop