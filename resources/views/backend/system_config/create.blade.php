@extends('backend.layout.default')

@section('title', "Thêm Cấu hình")



@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-8">

            <form method="POST" action="/backend/system_config" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Cấu hình</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="key" class="col-sm-2 control-label">Key</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="key" name="key"
                                                       placeholder="Key"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="value" class="col-sm-2 control-label">Giá trị</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="value" name="value"
                                                       placeholder="Giá trị"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả</label>

                                            <div class="col-sm-10">
                                            <textarea name="description" id="description"
                                                      class="form-control" placeholder="Mô tả"></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

@stop

@section('script.footer')

    @parent
@endsection
