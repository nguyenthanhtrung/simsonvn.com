@extends('backend.layout.default')

@section('title', $system_config->title)

@section('content')

    <div class="wrapper wrapper-content">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="150px">#</th>
                            <th>Giá trị</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ID</td>
                            <td>{!! $system_config->id !!}</td>
                        </tr>
                        <tr>
                            <td>Key</td>
                            <td>{!! $system_config->key !!}</td>
                        </tr>
                        <tr>
                            <td>Value</td>
                            <td>{!! $system_config->value !!}</td>
                        </tr>
                        <tr>
                            <td>Mô tả</td>
                            <td>{!! $system_config->description !!}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop