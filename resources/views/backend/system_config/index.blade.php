@extends('backend.layout.default')

@section('title', "Danh sách Cấu hình")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! action('Backend\SystemConfig@create') !!}" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> Thêm
                                </a>
                            </div>
                        </div>

                        @if($system_configs)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th>Key</th>
                                    <th class="col-sm-2">Giá trị</th>
                                    <th class="col-sm-2">Mô tả</th>
                                    <th class="col-sm-2">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0;?>
                                @foreach($system_configs as $system_config)
                                    <?php $i++;?>
                                    <tr>
                                        <td class="text-center">{!! $i !!}</td>
                                        <td>
                                            <strong>
                                                <a href="">{!! $system_config->key !!}</a>
                                            </strong>
                                        </td>
                                        <td>{!! $system_config->value !!}</td>
                                        <td>{!! $system_config->description !!}</td>
                                        <td width="150">
                                            <a class="btn btn-white btn-xs m-l-3"
                                               href="{!! route('system_config.show',['id'=>$system_config->id]) !!}"
                                               title="Xem">
                                                <i class="fa fa-search"></i> Xem
                                            </a>

                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('system_config.edit',['id'=>$system_config->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('system_config.destroy',['system_config'=>$system_config->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $system_configs->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop