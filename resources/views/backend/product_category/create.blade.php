@extends('backend.layout.default')

@section('title', "Thêm danh mục sản phẩm")

@section('style.header')

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-8">

            <form method="POST" action="/backend/product_category" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST" enctype="multipart/form-data">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">

                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Tên </label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="name" name="name"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Danh mục cha </label>

                                            <div class="col-sm-10">
                                                <select name="parent_id" class="form-control">
                                                    <option value="0">Là danh mục cha</option>
                                                    <?php
                                                        if(isset($parents) && !empty($parents)) {
                                                            foreach ($parents as $parent) {
                                                                ?>
                                                                <option value="<?php echo $parent->id;?>"><?php echo $parent->name;?></option>
                                                                <?php
                                                            }
                                                        }
                                                    ?>


                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả</label>

                                            <div class="col-sm-10">
                                            <textarea name="description" id="description"
                                                      class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="order"
                                                       name="order"  placeholder="Sắp xếp"
                                                       value="0">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Ảnh đại diện</label>

                                            <div class="col-sm-10">
                                                <input type="file" name="image" class="form-control"
                                                       accept="image/*"/>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i
                                                            class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

@stop

@section('script.footer')

    @parent
@endsection
