@extends('backend.layout.default')

@section('title', "Danh sách danh mục sản phẩm")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('product_category.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm danh mục sản phẩm</a>
                            </div>
                        </div>

                        @if($datas)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 60px">ID</th>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th>Thông tin chung</th>
                                    <th class="col-sm-1 text-center">Sắp xếp</th>
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-1 text-center">Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($datas as $tmp)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td class="text-center">
                                            <img style="width: 80px;height: 80px" src="{!! $tmp->image_url !!}" />
                                        </td>
                                        <td>
                                            <strong>
                                                <a href="{!! route('product_category.edit',['id'=>$tmp->id]) !!}">
                                                    {!! $tmp->name !!}
                                                </a>
                                            </strong>
                                        </td>

                                        <td class="text-center">{{$tmp->order}}</td>
                                        <td class="text-center">
                                            {{\App\Common\Utility::displayDatetime($tmp->created_time)}}
                                        </td>
                                        <td class="text-center" width="150">

                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('product_category.edit',['id'=>$tmp->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! url('backend/product_category/delete',['id'=>$tmp->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                    if(isset($tmp)) {
                                        $sp_categories = \App\Models\ProductCategory::query()
                                                ->orderBy('created_time', 'desc')
                                                ->where('parent_id',$tmp->id)->get();
                                        if(isset($sp_categories) && !empty($sp_categories)) {
                                            foreach ($sp_categories as $sp_category) {
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php $i++;echo $i;?></td>
                                                    <td class="text-center">
                                                        <img style="width: 80px;height: 80px" src="{!! $tmp->image_url !!}" />
                                                    </td>
                                                    <td>
                                                        <strong>
                                                            -------&nbsp;&nbsp;
                                                            <a href="{!! route('product_category.edit',['id'=>$sp_category->id]) !!}">
                                                                {!! $sp_category->name !!}
                                                            </a>
                                                        </strong>
                                                    </td>

                                                    <td class="text-center">{{$tmp->order}}</td>
                                                    <td class="text-center">
                                                        {{\App\Common\Utility::displayDatetime($sp_category->created_time)}}
                                                    </td>
                                                    <td class="text-center" width="150">

                                                        <a class="btn btn-warning btn-xs m-l-3"
                                                           href="{!! route('product_category.edit',['id'=>$sp_category->id]) !!}"
                                                           title="Sửa">
                                                            <i class="fa fa-pencil"></i> Sửa
                                                        </a>

                                                        <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                                           href="{!! url('backend/product_category/delete',['id'=>$sp_category->id]) !!}"
                                                           title="Xóa">
                                                            <i class="fa fa-trash"></i> Xóa
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }

                                    }
                                    ?>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $datas->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop