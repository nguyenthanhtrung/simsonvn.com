@extends('backend.layout.default')

@section('title', $data->code)

@section('content')

    <div class="wrapper wrapper-content">

        <div class="col-lg-12 no-padding">
            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <a href="{!! url('transaction') !!}" class="btn btn-sm btn-primary ">
                        Danh sách giao dịch
                    </a>
                    <table class="table table-bordered">
                        <tr>
                            <td class="col-sm-2">
                                <label>Trạng thái </label>
                            </td>
                            <td>
                                <strong>
                                    {!! \App\Models\Transaction::$statusTitleHtml[$data->status] !!}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-sm-2">
                                <label>Mã </label>
                            </td>
                            <td>{!! $data->code !!}</td>
                        </tr>
                        <tr>
                            <td><label>Khách hàng</label></td>
                            <td>{!! $data->account_name !!}</td>
                        </tr>

                        <tr>
                            <td>
                                <label>Số tiền</label>
                            </td>
                            <td>
                                <strong>{!! \App\Common\Utility::numberFormat($data->money) !!}</strong> VNĐ
                            </td>
                        </tr>

                        <tr>
                            <td><label>Thời gian GD thực tế</label></td>
                            <td>{!! \App\Common\Utility::displayDatetime($data->transaction_time) !!}</td>
                        </tr>
                        <tr>
                            <td><label>Thời gian tạo</label></td>
                            <td>{!! \App\Common\Utility::displayDatetime($data->created_time) !!}</td>
                        </tr>

                        <tr>
                            <td><label>Mô tả</label></td>
                            <td>{!! $data->description !!}</td>
                        </tr>
                        <?php if(isset($data) && $data->status == \App\Models\Transaction::STATUS_PENDING){?>
                            <tr>

                                <td colspan="2">
                                    <button data-id="{!! $data->id !!}" class="btn btn-sm btn-primary transaction_accept_btn">
                                        Duyệt giao dịch
                                    </button>
                                </td>
                            </tr>
                        <?php }?>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var accept_url="{!! url('transaction/change_status') !!}";
        $(document).ready(function() {
            $('.transaction_accept_btn').click(function() {
                if(confirm('Bạn có chắc chắn muốn thực hiện thao tác này ?') ) {
                     var id = $(this).attr('data-id');
                     $.ajax({
                         url : accept_url,
                         type : 'post',
                         data : {
                             id:id,
                             status:'ACCEPT'
                         },
                         success:function(result) {
                             if(result.status == 1) {
                                 alert('Duyệt thành công ');
                                 location.reload(true);
                                 return false;
                             }
                             alert(result.message);
                             return false;
                         }
                     });
                }

             });
        });
    </script>
@stop
