@extends('backend.layout.default')

@section('title', "Danh sách Giao dịch")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">

                            <form action="{!! url('transaction') !!}" method="get">
                                <div class="col-lg-12 no-padding">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-10">

                                        <div class="col-sm-2">
                                            <label>Thời gian từ</label>
                                            <input type="text" value="{!! request('date_from','') !!}" name="date_from" placeholder="từ" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>đến</label>
                                            <input type="text" value="{!! request('date_to','') !!}" name="date_to" placeholder="đến" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Khách hàng</label>
                                            <input type="text" value="{!! request('customer','') !!}" name="customer" placeholder="Khách hàng" class="form-control">
                                        </div>

                                        <div class="col-sm-2">
                                            <label>Loại giao dịch</label>
                                            <select name="transaction_type" class="form-control">
                                                <option value="">Tất cả</option>
                                                <option @if(request('transaction_type','') == 'deposit') selected @endif value="deposit">Nạp tiền</option>
                                                <option @if(request('transaction_type','') == 'paid') selected @endif value="paid">Thanh toán</option>
                                                <option @if(request('transaction_type','') == 'return') selected @endif value="return">Hoàn trả</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Trạng thái</label>
                                            <select name="status" class="form-control">
                                                <option value="">Tất cả</option>

                                                <option @if(request('status','') == 'PENDING') selected @endif
                                                    value="{!! \App\Models\Transaction::STATUS_PENDING !!}">
                                                    {!! \App\Models\Transaction::STATUS_PENDING !!}
                                                </option>
                                                <option @if(request('status','') == 'ACCEPT') selected @endif
                                                    value="{!! \App\Models\Transaction::STATUS_ACCEPT !!}">
                                                    {!! \App\Models\Transaction::STATUS_ACCEPT !!}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">

                                            <button class="btn btn-sm btn-primary" style="margin-top: 25px">
                                                <i class="fa fa-search"></i>
                                                Tìm kiếm
                                            </button>
                                        </div>


                                    </div>
                                </div>
                            </form>
                        </div>
                        <br/>
                        @if($datas)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">ID</th>
                                    <th>Mã giao dịch</th>
                                    <th class="col-sm-1 text-center">Khách hàng</th>
                                    <th class="col-sm-1 text-center">Loại GD</th>
                                    <th class="col-sm-1 text-right">Số tiền</th>
                                    <th class="col-sm-1 text-center">Trạng thái</th>
                                    <th class="col-sm-2 text-center">Thời gian Gd</th>
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-1 text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($datas as $data)
                                    <?php $i++;?>
                                    <tr>
                                        <td class="text-center">{!! $i !!}</td>
                                        <td>
                                            <strong>
                                                <a href="{!! route('transaction.show',['id'=>$data->id]) !!}">
                                                    {!! $data->code !!}
                                                </a>
                                            </strong>
                                        </td>
                                        <td class="text-center">
                                            {!! $data->account_name !!}
                                            ({!! $data->account_id !!})
                                        </td>
                                        <td class="text-center">
                                            {!! \App\Models\Transaction::getTypeTitleHtml($data->type) !!}

                                        </td>
                                        <td class="text-right text-money">
                                            @if($data->data_type == 'IN')
                                                <span class="text-success">
                                                + {{\App\Common\Number::money($data->money)}}
                                            </span>
                                            @elseif($data->data_type == 'OUT')
                                                <span class="text-danger">
                                                - {{\App\Common\Number::money($data->money)}}
                                            </span>
                                            @endif
                                        </td>
                                        <td class="text-center">{!! \App\Models\Transaction::$statusTitleHtml[$data->status] !!}</td>
                                        <td class="text-center">{!! \App\Common\Utility::displayDatetime($data->transaction_time) !!}</td>
                                        <td class="text-center">{!! \App\Common\Utility::displayDatetime($data->created_time) !!}</td>

                                        <td class="text-center">
                                            <a class="btn btn-primary btn-xs"
                                               href="{!! route('transaction.edit',['id'=>$data->id]) !!}"
                                               title="Xem">
                                                <i class="fa fa-folder"></i>
                                                Xem
                                            </a>

                                            <!--<a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('transaction.edit',['id'=>$data->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i>
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('transaction.destroy',['transaction'=>$data->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i>
                                            </a>-->
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="padding-left: 0">
                                <div class="col-sm-6" style="padding-left: 0">Tìm thấy tổng số {!! $datas->total() !!} kết quả</div>
                                <div class="col-sm-6">{!! $datas->render() !!}</div>
                            </div>


                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
