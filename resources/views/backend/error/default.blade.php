@extends('layout.error')

@section('title', "Console Error")

@section('content')

    <h1>{{isset($code)?$code:500}}</h1>

    <h3 class="font-bold">{{isset($message)?$message:'Error'}}</h3>

    <div class="error-desc">
        Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the
        refresh button on your browser or try found something else in our app.
    </div>

@stop