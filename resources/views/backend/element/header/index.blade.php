<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title')</title>

<link href="/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
<link href="/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/themes/inspinia/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="/themes/inspinia/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
<link href="/themes/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link rel="stylesheet" href="/themes/customer/plugins/daterangepicker/daterangepicker.css">
<link href="/themes/inspinia/css/animate.css" rel="stylesheet">

@section('style.header')
    <link href="/themes/inspinia/css/style.css" rel="stylesheet">
@show

<link rel="shortcut icon" href="/favicon.ico">

@section('script.header')

@show