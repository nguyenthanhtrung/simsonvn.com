<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">TungNT</strong>
                             </span>
                            </span>
                    </a>
                </div>
                <div class="logo-element">
                    CMS+
                </div>
            </li>
            <li>
                <a href="{!! url('backend/home') !!}"><i class="fa fa-th-large"></i> <span class="nav-label">Tổng quan</span></a>
            </li>
            <li class="active">
                <a href="#"><i class="fa fa-truck"></i> <span class="nav-label">Quản trị sản phẩm</span><span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{!! url('backend/product_category') !!}">Quản lý danh mục sp</a></li>
                    <li><a href="{!! url('backend/product') !!}">Quản lý sản phẩm</a></li>
                    {{--<li><a href="{!! url('backend/product_brand') !!}">Quản lý thương hiệu</a></li>--}}
                </ul>
            </li>

            <li class="active">
                <a href="#"><i class="fa fa-truck"></i> <span class="nav-label">Quản trị nội dung</span><span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{!! url('backend/post_category') !!}">Quản lý danh mục</a></li>
                    <li><a href="{!! url('backend/post') !!}">Quản lý bài viết</a></li>
                    <li><a href="{!! url('backend/video') !!}">Quản lý bài video</a></li>
                    <li><a href="{!! url('backend/banner') !!}">Quản lý banner</a></li>

                </ul>
            </li>
            <li>
                <a href="{!! url('backend/partner') !!}">
                    <i class="fa fa-steam"></i> <span class="nav-label">Đối tác</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/system_config') !!}">
                    <i class="fa fa-steam"></i> <span class="nav-label">Cấu hình</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/admin') !!}"><i class="fa fa-user"></i> <span class="nav-label">Quản trị viên</span></a>
            </li>
            <li>
                <a href="{!! url('backend/banner_sub') !!}"><i class="fa fa-image"></i> <span class="nav-label">Quản lý banner nhỏ</span> </a>
            </li>
            <li>
                <a href="{!! url('backend/order') !!}"><i class="fa fa-file"></i> <span class="nav-label">ĐƠN HÀNG</span> </a>
            </li>

        </ul>

    </div>
</nav>