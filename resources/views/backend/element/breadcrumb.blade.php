@if(isset($breadcrumb) && !empty($breadcrumb))
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{!! isset($breadcrumb['title'])?$breadcrumb['title']:'' !!}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Trang chủ</a>
                </li>
                @if(isset($breadcrumb['items']) && !empty($breadcrumb['items']))
                    @foreach($breadcrumb['items'] as $tmp)
                        <li>
                            @if(isset($tmp['url']) && $tmp['url']!='')
                                <a href="{!! $tmp['url'] !!}">{!! $tmp['title'] !!}</a>
                            @else
                                {!! $tmp['title'] !!}
                            @endif

                        </li>
                        <?php unset($tmp);?>
                    @endforeach
                @endif


            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endif