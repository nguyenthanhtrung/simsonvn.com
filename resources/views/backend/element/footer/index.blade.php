
<script src="/themes/inspinia/js/jquery-2.1.1.js"></script>
<script src="/themes/inspinia/js/base.js" rel="stylesheet"></script>


<script src="/themes/inspinia/js/bootstrap.min.js"></script>
<script src="/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="/themes/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="/themes/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/themes/inspinia/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="/themes/inspinia/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="/themes/inspinia/js/plugins/flot/jquery.flot.pie.js"></script>

<script src="/themes/inspinia/js/plugins/peity/jquery.peity.min.js"></script>
<script src="/themes/inspinia/js/demo/peity-demo.js"></script>

<script src="/themes/inspinia/js/inspinia.js"></script>
<script src="/themes/inspinia/js/plugins/pace/pace.min.js"></script>

<script src="/themes/inspinia/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/themes/inspinia/js/app.js" rel="stylesheet"></script>
<script src="/js/plugin/jquery.number.min.js"></script>

@section('script.footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.need_to_format').number(true,0);
        });
    </script>
@show