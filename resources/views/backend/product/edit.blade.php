@extends('backend.layout.default')

@section('title', "Sửa sản phẩm")

@section('style.header')
    <link href="/themes/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/themes/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-10">
            <?php
                if(isset($data) && !empty($data)) {
            ?>

                <input type="hidden" value="{!! $data->id !!}" id="product_id" />

                <div class="row">
                    <div class="tabs-container">

                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true">Thông tin chung</a>
                            </li>
                            {{--<li>
                                <a data-toggle="tab" href="#tab-2" aria-expanded="true">Giá sản phẩm</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab-3" aria-expanded="true">Thông số kỹ thuật</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab-4" aria-expanded="true">Khuyến mãi</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab-5" aria-expanded="true">Ảnh sản phẩm</a>
                            </li>--}}
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <form method="POST" action="/backend/product/{{$data->id}}" class="form-horizontal"
                                      enctype="multipart/form-data">

                                    <input name="_method" type="hidden" value="PATCH">

                                    {{csrf_field()}}
                                    @include('backend.product.el.product_general')
                                </form>
                            </div>
                            {{--<div id="tab-2" class="tab-pane">
                                @include('backend.product.el.product_price')
                            </div>
                            <div id="tab-3" class="tab-pane">
                                @include('backend.product.el.product_it')
                            </div>
                            <div id="tab-4" class="tab-pane">
                                @include('backend.product.el.product_promotion')
                            </div>
                            <div id="tab-5" class="tab-pane">
                                @include('backend.product.el.product_image')
                            </div>--}}

                        </div>

                    </div>

                </div>

            <?php }?>
        </div>
    </div>

@stop

@section('script.footer')
    @parent

    <script type="text/javascript">
        var url_list_product_price = "{!! action('Backend\ProductPrice@lists') !!}";
        var url_add_product_price = "{!! action('Backend\ProductPrice@add') !!}";
        var url_remove_product_price = "{!! action('Backend\ProductPrice@remove') !!}";

        var url_list_product_promotion = "{!! action('Backend\ProductPromotion@lists') !!}";
        var url_add_product_promotion = "{!! action('Backend\ProductPromotion@add') !!}";
        var url_remove_product_promotion = "{!! action('Backend\ProductPromotion@remove') !!}";

        var url_add_product_it = "{!! action('Backend\ProductIt@add') !!}";

        var url_upload_image_file = "{!! action('Backend\ProductImage@add') !!}";
        var url_list_images = "{!! action('Backend\ProductImage@lists') !!}";
        var url_remove_product_image = "{!! action('Backend\ProductImage@remove') !!}";
    </script>
    <script src="/cms/js/product.js"></script>

    <script src="/themes/inspinia/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);
                data.append("type", '{{\App\Common\UploadedImage::POST_EDITOR}}');

                $.ajax({
                    type: "POST",
                    url: "/backend/image/editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>

@endsection
