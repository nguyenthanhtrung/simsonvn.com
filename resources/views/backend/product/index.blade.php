@extends('backend.layout.default')

@section('title', "Danh sách sản phẩm")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('product.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm sản phẩm</a>
                            </div>
                        </div>

                        @if($datas)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px">ID</th>
                                    <th class="text-center" style="width: 82px">Ảnh</th>
                                    <th>Sản phẩm</th>
                                    <th class="col-sm-2 text-center">Giá bán (vnđ)</th>
                                    <th class="col-sm-1 text-center">Danh mục</th>
                                    {{--<th class="col-sm-1 text-center">Thương hiệu</th>--}}
                                    <th class="col-sm-2 text-center">Thời gian tạo</th>
                                    <th class="col-sm-1 text-center">Trạng thái</th>
                                    <th class="col-sm-1 text-center">Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($datas as $tmp)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td class="text-center">
                                            <img src="{!! $tmp->image !!}" style="width: 80px;height: 80px" />
                                        </td>
                                        <td>

                                            <a href="{!! route('product.edit',['id'=>$tmp->id]) !!}">
                                                <strong>{!! $tmp->name !!}</strong>
                                            </a>
                                            <br/>
                                            {!! $tmp->slug !!}
                                        </td>
                                        <td class="text-center">
                                            {!! \App\Common\Utility::numberFormat($tmp->price).' / '.
                                            \App\Common\Utility::numberFormat($tmp->price_promotion) !!}
                                        </td>
                                        <td class="text-center">
                                            {!! $tmp->product_category_name !!}
                                        </td>
                                        {{--<td class="text-center">
                                            {!! $tmp->product_brand_name !!}
                                        </td>--}}
                                        <td class="text-center">
                                            {{\App\Common\Utility::displayDatetime($tmp->created_time)}}
                                        </td>
                                        <td class="text-center">
                                            {{ ($tmp->status == 1 ? 'Hiện' : 'Ẩn') }}
                                        </td>
                                        <td class="text-center" width="150">

                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('product.edit',['id'=>$tmp->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! url('backend/product/delete',['id'=>$tmp->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $datas->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop