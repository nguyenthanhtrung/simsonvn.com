<?php
    $i=0;
    if(isset($product_images)) {
        foreach ($product_images as $product_image) {
            $i++;
            ?>
            <tr>
                <td class="text-center"><?php echo $i;?></td>
                <td>
                    <div class="col-sm-5">
                        <img src="<?php echo $product_image->url;?>" style="width: 80px;height:80px" />
                    </div>
                    <div class="col-sm-7">
                        <?php echo $product_image->name;?>
                        <br/>
                        <?php echo \App\Common\Utility::displayDatetime($product_image->created_time);?>
                    </div>

                </td>

                <td class="text-center"><?php echo $product_image->order;?></td>
                <td class="text-center"><?php echo $product_image->default;?></td>
                <td class="text-center">
                    <a data-id="{!! $product_image->id !!}" class="btn btn-xs btn-danger remove_product_image" href="">Xóa</a>
                </td>
            </tr>
            <?php
        }
    }
?>