<?php
    $i=0;
    if(isset($product_prices)) {

        foreach ($product_prices as $product_price) {
            $i++;
            ?>
            <tr>
                <td class="text-center"><?php echo $i;?></td>
                <td><?php echo $product_price->color;?></td>
                <td class="text-center">
                    <?php echo \App\Common\Utility::numberFormat($product_price->price);?>
                </td>
                <td><?php echo $product_price->order;?></td>
                <td class="text-center">
                    <a data-id="{!! $product_price->id !!}" class="btn btn-xs btn-danger remove_product_price" href="">Xóa</a>
                </td>
            </tr>
            <?php
        }
    }
?>

