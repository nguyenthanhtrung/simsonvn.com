<div class="panel-body">
    <div class="col-lg-12">

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Tên </label>

            <div class="col-sm-10">
                <input type="text" class="form-control"  name="name" value="{!! isset($data)?$data->name:'' !!}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Danh mục sản phẩm</label>

            <div class="col-sm-10">
                <select name="product_category" class="form-control">
                    <option value="">Tất cả</option>
                    <?php
                        if(isset($product_categories) && !empty($product_categories)) {
                            foreach ($product_categories as $tmp) {
                                ?>
                                <option
                                        <?php if(isset($data) && $data->product_category_id == $tmp->id){
                                            echo 'selected';
                                        }?>
                                        value="<?php echo $tmp->id;?>"><?php echo $tmp->name;?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Giá hiển thị</label>

            <div class="col-sm-10">
                <input type="text" class="form-control need_to_format" name="price"
                       value="<?php if(isset($data)) {echo $data->price;}?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Giá khuyến mãi</label>

            <div class="col-sm-10">
                <input type="text" class="form-control need_to_format" name="price_promotion"
                       value="<?php if(isset($data)) {echo $data->price_promotion;}?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Số lượng còn</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="stock"
                       value="<?php if(isset($data)) {echo $data->stock;}?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Hiển thị ở trang chủ</label>

            <div class="col-sm-10">
                <input type="checkbox" <?php if(isset($data) && $data->display_in_home == 1) {
                    echo 'checked';
                }?> name="display_in_home" value="1" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Mô tả</label>

            <div class="col-sm-10">
                <textarea class="form-control summernote" name="description"><?php if(isset($data)){ echo $data->description;}?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Ảnh</label>

            <div class="col-sm-10">
                <input type="file" id="file" name="image" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Trạng thái</label>

            <div class="col-sm-10">
                <input type="radio" name="status" value="1"
                <?php if(isset($data) && $data->status == 1) {
                    echo 'checked';
                }?>
                /> Hoạt động
                &nbsp;&nbsp;&nbsp;
                <input type="radio" name="status" value="0"
                <?php if(isset($data) && $data->status == 0) {
                    echo 'checked';
                }?>
                /> Không hoạt động
            </div>
        </div>
        <div class="form-group ">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <button class="btn btn-primary" type="submit"><i
                            class="fa fa-check"></i> Cập nhật
                </button>
                <button class="btn btn-default" type="reset">Làm lại</button>
            </div>
        </div>

    </div>
</div>