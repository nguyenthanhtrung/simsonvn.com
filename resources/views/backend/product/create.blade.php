@extends('backend.layout.default')

@section('title', "Thêm sản phẩm")

@section('style.header')
    <link href="/themes/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/themes/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-10">

            <form method="POST" action="/backend/product" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST" enctype="multipart/form-data">

                {{csrf_field()}}

                <div class="row">
                    <div class="tabs-container">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                @include('backend.product.el.product_general')
                            </div>
                        </div>

                    </div>

                </div>

            </form>

        </div>
    </div>

@stop

@section('script.footer')
    <script src="/themes/inspinia/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);
                data.append("type", '{{\App\Common\UploadedImage::POST_EDITOR}}');

                $.ajax({
                    type: "POST",
                    url: "/backend/image/editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>

    @parent
@endsection
