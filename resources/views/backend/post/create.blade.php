@extends('backend.layout.default')

@section('title', "Thêm Bài viết")

@section('style.header')
    <link href="/themes/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/themes/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    @parent
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">

            <form method="POST" action="/backend/post" class="form-horizontal"
                  enctype="multipart/form-data">

                <input name="_method" type="hidden" value="POST">

                {{csrf_field()}}
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-lg-12">
                                <br>
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Bài viết</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Tên Bài viết"
                                               value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="category_id" class="col-sm-2 control-label">Danh mục</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" name="category_id">
                                            <option value="0">Chọn Danh mục</option>
                                            @if(isset($post_categories))
                                                @foreach($post_categories as $key=>$value)
                                                    <option value="{{$key}}">
                                                        {{$value}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="introduce" class="col-sm-2 control-label">Mô tả</label>

                                    <div class="col-sm-10">
                                <textarea name="introduce" id="introduce"
                                          class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="content" class="col-sm-2 control-label">Nội dung</label>

                                    <div class="col-sm-10">
                                <textarea name="content" id="content"
                                          class="form-control summernote"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">Trạng
                                        thái</label>

                                    <div class="col-sm-10">
                                        <input type="checkbox" name="status"
                                               id="status">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="order" class="col-sm-2 control-label">Sắp xếp</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="order"
                                               name="order" placeholder="Sắp xếp"
                                               value="0">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ảnh đại diện</label>

                                    <div class="col-sm-10">
                                        <input type="file" name="thumb" class="form-control"
                                               accept="image/*"/>
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <button class="btn btn-primary" type="submit"><i
                                                    class="fa fa-check"></i> Cập nhật
                                        </button>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@stop

@section('script.footer')
    <script src="/themes/inspinia/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['table', 'picture', 'link', 'video']],
                    ['misc', ['fullscreen', 'codeview', 'help']]
                ],
                onImageUpload: function (files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);
                data.append("type", '{{\App\Common\UploadedImage::POST_EDITOR}}');

                $.ajax({
                    type: "POST",
                    url: "/backend/image/editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>

    @parent
@endsection
