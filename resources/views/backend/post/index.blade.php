@extends('backend.layout.default')

@section('title', "Danh sách bài viết")

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{!! route('post.create') !!}" class="btn btn-primary"><i
                                            class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>

                        @if($posts)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 70px">ID</th>
                                    <th class="col-sm-1 text-center">Ảnh đại diện</th>
                                    <th>Bài viết</th>
                                    <th class="col-sm-1 text-center">Trạng thái</th>
                                    <th class="col-sm-1 text-center">Sắp xếp</th>
                                    <th class="col-sm-1 text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;?>
                                @foreach($posts as $post)
                                    <tr>
                                        <td class="text-center"><?php $i++;echo $i;?></td>
                                        <td>
                                            <img src="{!! $post->image !!}" alt="{!! $post->title !!}"
                                                 style="max-height: 100px; max-width: 100px;">
                                        </td>
                                        <td>
                                            <a href="{!! route('post.edit',['id'=>$post->id]) !!}"
                                               target="_blank">{!! $post->title !!}
                                            </a>
                                            @if($post->homepage == 1)<br/>Hiển thị trang chủ@endif
                                        </td>
                                        <td class="text-center">
                                            @if($post->status == 1)
                                                <span class="text-success">ACTIVE</span>
                                            @else
                                                <span class="text-warning">INACTIVE</span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            {!! $post->order !!}
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-warning btn-xs m-l-3"
                                               href="{!! route('post.edit',['id'=>$post->id]) !!}"
                                               title="Sửa">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>

                                            <a class="btn btn-danger btn-xs m-l-3 _delete_item"
                                               href="{!! route('post.destroy',['id'=>$post->id]) !!}"
                                               title="Xóa">
                                                <i class="fa fa-trash"></i> Xóa
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $posts->render() !!}

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
