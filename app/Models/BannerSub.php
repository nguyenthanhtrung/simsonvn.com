<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerSub extends Model
{

    /**
     * @var string
     */
    protected $table = 'banner_sub';
    protected $guarded = [];
    /**
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';



}
