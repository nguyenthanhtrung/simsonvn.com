<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{

    /**
     * @var string
     */
    protected $table = 'system_config';
    /**
     * @var bool
     */
    protected $guarded = [];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';
    
}
