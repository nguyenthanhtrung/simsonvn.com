<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];
    protected $table = 'transaction';

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    const STATUS_PENDING = 'PENDING';
    const STATUS_ACCEPT = 'ACCEPT';

    static $statusTitle = [
        self::STATUS_PENDING => 'Chờ duyệt',
        self::STATUS_ACCEPT  => 'Đã duyệt'
    ];

    static $statusTitleHtml = [
        self::STATUS_PENDING => '<span style="color:red">Chờ duyệt</span>',
        self::STATUS_ACCEPT  => '<span style="color:green">Đã duyệt</span>'
    ];

    static $typeTitle = [
        'deposit'=>'Nạp tiền',
        'paid'=>'Thanh toán',
        'return'=>'Hoàn trả'
    ];
    static $typeTitleHtml = [
        'deposit'=>'Nạp tiền',
        'paid'=>'Thanh toán',
        'return'=>'Hoàn trả'
    ];


    public static function getTypeTitleHtml($type) {
        return isset(self::$typeTitleHtml[$type])?self::$typeTitleHtml[$type]:'';
    }
}
