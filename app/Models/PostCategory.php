<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{

    /**
     * @var string
     */
    protected $table = 'post_category';
    /**
     * @var bool
     */
    public $timestamps = false;

    public function url()
    {
        return \App\Models\Helper\PostCategory::getUrl($this->slug);
    }

    public function products()
    {
        return Post::query()->where('category_id', $this->id)
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->orderBy('created_time', 'desc')
            ->limit(5)
            ->get();
    }
}
