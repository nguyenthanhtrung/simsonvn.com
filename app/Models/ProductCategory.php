<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    /**
     * @var string
     */
    protected $table = 'product_category';

    protected $guarded = [];
    /**
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    public static function getLink($first, $second = null)
    {
        $params = [];

        if (isset($first)) {
            $params[] = $first->slug;
        }

        if (isset($second) && $second instanceof self) {
            $params[] = $second->slug;
        }

        return action('Frontend\Product@index', $params);
    }

    public function categories()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id')->orderBy('order', 'asc');
    }
}
