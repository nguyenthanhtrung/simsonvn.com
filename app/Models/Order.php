<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /**
     * @var string
     */
    protected $table = 'order';
    protected $guarded = [];
    /**
     * @var bool
     */
    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    static $statusLists = [
	    'PENDING'=>'<span style="color:red">Chưa xử lý</span>',
	    'DONE'=>'<span style="color:green">Đã xử lý</span>'
    ];

	public function displayTitle() {
		$key = $this->status;
		return self::$statusLists[$key];
	}
}
