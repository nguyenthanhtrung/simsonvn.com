<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * @var string
     */
    protected $table = 'post';
    protected $guarded = [];
    /**
     * @var bool
     */
    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    
}
