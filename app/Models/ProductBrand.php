<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{

    /**
     * @var string
     */
    protected $table = 'product_brand';
    protected $guarded = [];
    /**
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';


}
