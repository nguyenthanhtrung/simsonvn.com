<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = [];
    protected $table = 'account';

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    
}
