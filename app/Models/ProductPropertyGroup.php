<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyGroup extends Model
{

    /**
     * @var string
     */
    protected $table = 'product_property_group';
    /**
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';


}
