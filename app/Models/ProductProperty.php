<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{

    /**
     * @var string
     */
    protected $table = 'product_property';
    /**
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';


}
