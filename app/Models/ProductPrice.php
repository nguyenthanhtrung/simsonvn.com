<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{

    /**
     * @var string
     */
    protected $table = 'product_price';
    /**
     * @var bool
     */
    public $timestamps = true;
    protected $guarded = [];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

}
