<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * @var string
     */
    protected $table = 'product';
    /**
     * @var bool
     */
    public $timestamps = true;
    protected $guarded = [];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

    static $colors = [
        'VANG'=>'Màu vàng gold',
        'HONG'=>'Màu hồng',
        'DEN'=>'Màu đen',
        'TRANG'=>'Màu trắng',
        'GHI'=>'Màu ghi',
	    'BLUE'=>'Màu xanh',
	    'PURPLE'=>'Màu tím'
    ];

    public static function getColors() {
        return self::$colors;
    }

    public static function getReward() {
        return [
                'Tặng dán Cường lực, Ốp lưng khi mua BHV',
                'Tặng Voucher Giảm giá mua máy & sửa chữa trị giá 50.000đ',
                'Giảm ngay 100K mang theo thẻ HSSV tại 398 Cầu Giấy',
                'Sạc DP 8000mAh 300K, Tai nghe Bluetooth 150K, Gậy TS 100K',
                'Mua Combo 3 món trên chỉ với giá #299K tại MobileCity',
        ];
    }

    public static function getLink($product) {
        return url('/san-pham/'.$product->slug.'.html');
        //return action('Frontend\Product@detail',[$product->slug]);
    }
}
