<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'admin';
    /* dung update_at va created_at ko*/
    public $timestamps = false;
    protected $guarded = [];


}
