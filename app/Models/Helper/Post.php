<?php
namespace App\Models\Helper;


/**
 * Class Post
 * @package App\Model\Helper
 */
class Post
{
    /**
     * @param $id
     * @return mixed
     */
    public static function findById($id)
    {
        $post = \App\Models\Post::query()->where('active', 1)->findOrFail($id);

        return self::rebuildData($post);
    }


    public static function getUrl($slug, $id)
    {
        return action('Frontend\Post@show', ['id' => $id, 'slug' => $slug]);
    }
}