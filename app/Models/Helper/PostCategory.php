<?php

namespace App\Models\Helper;

/**
 * Class Post
 * @package App\Model\Helper
 */
class PostCategory
{
    public static function getUrl($slug)
    {
        return action('Frontend\Post@index', ['slug' => $slug]);
    }

    public static function getList($id = 0)
    {
        if ($id != 0) {
            $post_categories = \App\Models\PostCategory::query()
                ->where('parent_id', 0)
                ->where('id', '!=', $id)
                ->orderBy('title', 'asc')
                ->get();
        } else {
            $post_categories = \App\Models\PostCategory::query()
                ->where('parent_id', 0)
                ->orderBy('title', 'asc')
                ->get();
        }

        if (!empty($post_categories)) {
            $post_categories = $post_categories->toArray();
            $post_categories = array_pluck($post_categories, 'title', 'id');
        } else {
            $post_categories = [];
        }

        return $post_categories;
    }
}