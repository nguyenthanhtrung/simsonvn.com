<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{

    /**
     * @var string
     */
    protected $table = 'partner';
    protected $guarded = [];
    /**
     * @var bool
     */
    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'updated_time';

}
