<?php

namespace App\Handlers\Event;


use App\Events\TransactionSynchronize;
use App\Models\Account;
use App\Models\Transaction;

class CalcUserBalance
{

    public function handle(TransactionSynchronize $event)
    {
        $data = $event->data;
        $transaction_id = $data['id'];
        $transaction = Transaction::query()->where('id',$transaction_id)->first();

        $balance = \App\Models\Helper\Transaction::calcAccountBalance($transaction->account_id);
        $account = Account::query()->where('id', $transaction->account_id)->first();
        $account->update([
            'balance'=>$balance
        ]);
    }
}
