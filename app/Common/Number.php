<?php
namespace App\Common;


class Number
{
    public static function money($data)
    {
        return number_format($data, 0, '', '.');
    }
}