<?php
namespace App\Common;

class UploadedFile
{
    /**
     * @var
     */
    protected $file;

    /**
     * @var mixed
     */
    protected $baseDir;
    /**
     * @var
     */
    protected $baseUrl;
    /**
     * @var
     */
    protected $pathDir;
    /**
     * @var
     */
    protected $fileName;

    /**
     * @var
     */
    protected $extension;

    /**
     * Upload constructor.
     */
    public function __construct($file)
    {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile)
            $this->file = $file;
        else
            throw new \Exception('Is not class: UploadedFile!');

        $this->setBaseDir(env('UPLOAD_PATH'));
        $this->setBaseUrl(env('WEB_PATH'));
    }

    /**
     * @return mixed
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }

    /**
     * @param mixed $baseDir
     */
    public function setBaseDir($baseDir)
    {
        $this->baseDir = $baseDir;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getPathDir()
    {
        return $this->pathDir;
    }

    /**
     * @param mixed $pathDir
     */
    public function setPathDir($pathDir)
    {
        $this->pathDir = $pathDir;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     *
     */
    public function move()
    {
        $this->file->move($this->getBaseDir() . DIRECTORY_SEPARATOR .
            $this->getPathDir(), $this->getFileName() . '.' . $this->getExtension());
    }
}