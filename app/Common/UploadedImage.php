<?php

namespace App\Common;

class UploadedImage extends UploadedFile
{

    const POST_EDITOR = 'img.editor.post';
    const PRODUCT_EDITOR = 'img.editor.product';

    /**
     * UploadedImage constructor.
     */
    public function __construct($file)
    {
        parent::__construct($file);

        if ($file->getClientMimeType() != 'image/jpeg' && $file->getClientMimeType() != 'image/png')
            throw new \Exception('Is not Image!');
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return empty($this->fileName) ? uniqid() : $this->fileName;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return empty($this->extension) ? 'jpg' : $this->extension;
    }

}