<?php
namespace App\Common;



/**
 * Class Utility
 * @package App\Commons
 */
class Utility
{

    const POST_ENCODE = 4;

    /**
     * @todo phân chia phần số nguyên
     * @param $value
     * @param int $min_money
     * @param null $symbol
     * @return float|int|mixed|string
     */
    public static function numberFormat($value, $min_money = 1000, $symbol = null)
    {
        $value = intval($value);
        $small_than_zero = false;
        if(floatval($value) < 0) {
            $value = $value*-1;
            $small_than_zero = true;
        }

        $value = $value < $min_money ?
            $value : ($value / $min_money) * $min_money;
        if (intval($value) >= $min_money) {
            if ($value != '' and is_numeric($value)) {
                $value = number_format($value, 2, ',', '.');
                $value = str_replace(',00', '', $value);
            }
        }
        if ($symbol){
            $value .= ' ' . $symbol;
        }
        if($small_than_zero == true) {
            return '-'.$value;
        }
        return $value;
    }


    /**
     * @todo cắt chuỗi theo số từ
     * @param $str
     * @param $len
     * @param string $pad
     * @param bool|FALSE $strip
     * @return mixed|string
     */
    public static function strLeft($str, $len, $pad = '...', $strip = FALSE)
    {
        if (strlen($str) <= $len)
            return $str;
        if ($strip)
            $str = strip_tags($str);
        $txt = substr($str, 0, $len);
        $pos = strrpos($txt, ' ');
        return substr_replace($txt, $pad, $pos + 1);
    }

    /**
     * @todo lấy ra thời gian hiện tại, phù hợp sử dụng cho insert, update
     * @return bool|string
     */
    public static function timeNow()
    {
        return date('Y-m-d H:i:s', time());
    }

    /**
     * @todo Hiển thị thời gian hiện tại
     * @param $time
     * @param string $format
     * @return bool|string
     */
    public static function displayDatetime($time, $format = 'H:i d/m/Y')
    {
        if ($time == '00:00:00 0000:00:00'
            || $time == '0000:00:00 00:00:00'
            || $time == '0000-00-00 00:00:00'
            || $time == ''
            || $time == null
            || $time == 'null'
        ) {
            return '';
        }
        if (is_numeric($time)) {
            return date($format, $time);
        }
        return date($format, strtotime($time));
    }

    /**
     * @param $s
     * @return string
     */
    public static function secondToTime($s)
    {
        $hour = floor($s / 3600);
        $minute = floor(($s - $hour * 3600) / 60);
        $second = $s - $hour * 3600 - $minute * 60;

        if ($hour == 0) {
            $hour = '';
        } else if ($hour < 10) {
            $hour = '0' . $hour . ':';
        } else {
            $hour = $hour . ':';
        }

        if ($minute == 0) {
            $minute = '00:';
        } else if ($minute < 10) {
            $minute = '0' . $minute . ':';
        } else {
            $minute = $minute . ':';
        }

        if ($second == 0) {
            $second = '00';
        } else if ($second < 10) {
            $second = '0' . $second;
        }

        return $hour . $minute . $second;
    }

    /**
     * @param $time
     * @return string
     */
    public static function makeFriendlyTime($time)
    {
        $now = date("Y-m-d H:i:s");

        $secondsToNow = round(strtotime($now) - strtotime($time));

        if ($secondsToNow < 60) {
            return "$secondsToNow giây trước";
        } else if ($secondsToNow >= 60 && $secondsToNow < 3600) {
            $minutes = round($secondsToNow / 60);
            return "$minutes phút trước";
        } else if ($secondsToNow > 3600 && $secondsToNow < 86400) {
            $hours = round($secondsToNow / 3600);
            return "$hours giờ trước";
        } else if ($secondsToNow >= 86400 && $secondsToNow < 2592000) {
            $days = round($secondsToNow / 86400);
            return "$days ngày trước";
        } else if ($secondsToNow >= 2592000 && $secondsToNow < 31104000) {
            $months = round($secondsToNow / 2592000);
            return "$months tháng trước";
        } else {
            $years = round($secondsToNow / 31104000);
            return "$years năm trước";
        }
    }

    /**
     * @param $namespace
     * @param array $except
     * @return array
     */
    public static function getClassOnNamespace($namespace, $except = [])
    {
        if (null == $namespace) return [];
        $class = get_declared_classes();
        $data = [];

        if (isset($class) && !empty($class)) {

            foreach ($class as $tmp) {
                if (strpos($tmp, $namespace) !== false) {
                    $data[] = $tmp;
                }
            }
        }
        return array_diff($data, $except);

    }

    /**
     * @param $class_name
     * @param null $except_class
     * @return array
     */
    public static function getMethodsInClass($class_name, $except_class = null)
    {
        $allMethods = get_class_methods($class_name);
        $exceptMethods = [];
        if ($except_class !== null) {
            $exceptMethods = get_class_methods($except_class);
        }
        return array_diff($allMethods, $exceptMethods);
    }


    /**
     * @todo hàm bỏ dấu
     * @param $string
     * @return mixed
     */
    public static function stripText($string)
    {
        $from = array("à", "ả", "ã", "á", "ạ", "ă", "ằ", "ẳ", "ẵ", "ắ", "ặ", "â", "ầ", "ẩ", "ẫ", "ấ", "ậ", "đ", "è", "ẻ", "ẽ", "é", "ẹ", "ê", "ề", "ể", "ễ", "ế", "ệ", "ì", "ỉ", "ĩ", "í", "ị", "ò", "ỏ", "õ", "ó", "ọ", "ô", "ồ", "ổ", "ỗ", "ố", "ộ", "ơ", "ờ", "ở", "ỡ", "ớ", "ợ", "ù", "ủ", "ũ", "ú", "ụ", "ư", "ừ", "ử", "ữ", "ứ", "ự", "ỳ", "ỷ", "ỹ", "ý", "ỵ", "À", "Ả", "Ã", "Á", "Ạ", "Ă", "Ằ", "Ẳ", "Ẵ", "Ắ", "Ặ", "Â", "Ầ", "Ẩ", "Ẫ", "Ấ", "Ậ", "Đ", "È", "Ẻ", "Ẽ", "É", "Ẹ", "Ê", "Ề", "Ể", "Ễ", "Ế", "Ệ", "Ì", "Ỉ", "Ĩ", "Í", "Ị", "Ò", "Ỏ", "Õ", "Ó", "Ọ", "Ô", "Ồ", "Ổ", "Ỗ", "Ố", "Ộ", "Ơ", "Ờ", "Ở", "Ỡ", "Ớ", "Ợ", "Ù", "Ủ", "Ũ", "Ú", "Ụ", "Ư", "Ừ", "Ử", "Ữ", "Ứ", "Ự", "Ỳ", "Ỷ", "Ỹ", "Ý", "Ỵ");
        $to = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "d", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "D", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y");
        return str_replace($from, $to, $string);
    }

    /**
     * @todo xóa ký tự đặc biệt
     * @param $string
     * @return string
     */
    public static function cleanUpSpecialChars($string)
    {
        $string = preg_replace(array("`\W`i", "`[-]+`"), "-", $string);
        return trim($string, '-');
    }

    /**
     * @todo tạo slug
     * @param $string
     * @return string
     */
    public static function makeSlug($string)
    {
        $title = self::stripText(strtolower($string));
        $title =  self::cleanUpSpecialChars($title);
        $title = strtolower($title);
        return $title;
    }

    
    /**
     * @param $objId
     * @param bool|false $isDir
     * @return string
     */
    public static function storageSolutionEncode($objId, $isDir = false)
    {
        $step = 15; // So bit de ma hoa ten thu muc tren 1 cap
        $layer = 3; // So cap thu muc
        $max_bits = PHP_INT_SIZE * 8;
        $result = "";

        for ($i = $layer; $i > 0; $i--) {
            $shift = $step * $i;
            $layer_name = $shift <= $max_bits ? $objId >> $shift : 0;

            $result .= $isDir ? DS . $layer_name : "/" . $layer_name;
        }

        return $result;
    }

    /**
     * @return string
     */
    public static function url_current()
    {
        $isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
        $port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
        $port = ($port) ? ':' . $_SERVER["SERVER_PORT"] : '';
        $url = ($isHTTPS ? 'https://' : 'http://') . $_SERVER["SERVER_NAME"] . $port . $_SERVER["REQUEST_URI"];
        return $url;
    }

    public static function makeDirectory($dir, $mode = 0777, $recursive = true)
    {
        if (!file_exists($dir)) {
            $old_umask = umask(0);
            mkdir($dir, $mode, $recursive);
            umask($old_umask);
        }
    }
}