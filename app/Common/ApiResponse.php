<?php
namespace App\Common;

class ApiResponse extends \stdClass{

    public static function response($error, $message, $data = []) {
        return response(json_encode([
            'code'=>$error,
            'message'=>$message,
            'data'=>$data
        ]),200)->header('Content-Type', 'application/json; charset=utf-8');

    }
    public static function responseSuccess($message, $data = []) {

        return response(json_encode([
            'code'=>0,
            'message'=>$message,
            'data'=>$data
        ]),200)->header('Content-Type', 'application/json; charset=utf-8');

    }
    public static function responseError($message, $data = [], $code = 1) {
        return response(json_encode([
            'code'=>$code,
            'message'=>$message,
            'data'=>$data
        ]),200)->header('Content-Type', 'application/json; charset=utf-8');

    }
}