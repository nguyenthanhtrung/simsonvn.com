<?php
namespace App\Http\Controllers\Frontend;


use App\Models\Banner;
use App\Models\ProductCategory;

class Error extends Base
{
    public function not_found() {
        return 'not found item';
    }
}