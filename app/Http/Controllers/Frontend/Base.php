<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\PostCategory;
use App\Models\ProductCategory;
use App\Models\SystemConfig;

class Base extends Controller
{
    public function __construct()
    {
        $post_categories = PostCategory::query()
	        ->orderBy('order','asc')
	        ->get();

        view()->share('post_categories', $post_categories);

        $main_product_categories = ProductCategory::query()
            ->with(['categories'])
            ->where('parent_id', 0)
            ->orderBy('order', 'asc')
            ->get();

        view()->share('main_product_categories', $main_product_categories);

        $footer_posts = \App\Models\Post::query()
            ->where('status', 1)
            ->where('homepage', 1)
            ->orderBy('order', 'asc')
            ->limit(4)
            ->get();

        view()->share('footer_posts', $footer_posts);

        $banners = Banner::query()->orderBy('order', 'asc')->get();

        view()->share('banners', $banners);

        $system_configs = SystemConfig::query()->get();
        $system_configs = array_pluck($system_configs->toArray(), 'value', 'key');

        view()->share('system_configs', $system_configs);
    }
}