<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Location;
use App\Models\ProductCategory;
use App\Models\ProductIt;
use App\Models\ProductPrice;
use App\Models\ProductPromotion;

class Product extends Base
{
    public function index($main_category, $sub_category = null)
    {
        $main_category = ProductCategory::query()->where('slug', $main_category)->first();
        if (!isset($main_category) || empty($main_category)) {
            return action('Frontend\Error@not_found');
        }
        $category_ids = [];
        if ($sub_category == null) {
            $sub_categories = ProductCategory::query()
                ->where('parent_id', $main_category->id)
                ->get();
            $main_category_id = $main_category->id;
            $category_ids[] = $main_category->id;
        } else {
            $sub_category = ProductCategory::query()
                ->where('slug', $sub_category)
                ->where('parent_id', $main_category->id)
                ->first();
            $main_category_id = $sub_category->parent_id;
            $sub_categories = [$sub_category];

        }
        if (isset($sub_categories) && !empty($sub_categories)) {
            foreach ($sub_categories as $tmp) {
                $category_ids[] = $tmp->id;
            }
        }
        //

        $filter_sub_categories = ProductCategory::query()
            ->where('parent_id', $main_category_id)
            ->get();
        $filter_prices = [
            '0 - 3.000.000',
            '3.000.000 - 5.000.000',
            '5.000.000 - 25.000.000',
        ];

        //

        $query = \App\Models\Product::query()
            ->whereIn('product_category_id', $category_ids);


        $price_from = request()->get('price_from', '');
        $price_to = request()->get('price_to', '');
        //echo $price_from.'_'.$price_to;exit;
        if ($price_from != '' && $price_to != '') {

            $price_from = str_replace('.', '', $price_from);
            $price_to = str_replace('.', '', $price_to);

            $query
                ->where('price', '>=', $price_from)
                ->where('price', '<=', $price_to);
        }

        $o = request()->get('o', '');
        if (isset($o) && !empty($o)) {
            if ($o == 'lth') {
                $query->orderBy('price', 'asc');
            } else if ($o == 'htl') {
                $query->orderBy('price', 'desc');
            }
        }

        $products = $query->paginate(100);

        return view('frontend.product.index', compact(
            'products',
            'main_category',
            'sub_category',
            'filter_prices',
            'filter_sub_categories'
        ));
    }

    public function detail($slug)
    {

        $product = \App\Models\Product::query()->where('slug', $slug)->first();
        $locations = Location::query()
            ->where('type', 'STATE')
            ->where('ordering', 'asc')
            ->get();

        $product_same_prices = \App\Models\Product::query()
            ->where('price', '>', $product->price - 500000)
            ->where('price', '<', $product->price + 500000)
            ->orderBy('price', 'desc')
            ->take(6)
            ->skip(0)
            ->get();
        $product_relations = \App\Models\Product::query()
            ->where('product_category_id', $product->product_category_id)
            ->orderBy('created_time', 'desc')
            ->offset(0)
            ->limit(3)
            ->get();

        /*$product_it = ProductIt::query()->where('product_id', $product->id)->first();
        $product_promotions = ProductPromotion::query()
            ->where('product_id', $product->id)
            ->orderBy('order', 'asc')
            ->get();*/

        /*$product_prices = ProductPrice::query()
            ->where('product_id', $product->id)
            ->orderBy('order', 'asc')
            ->get();*/

        $product_category_id = $product->product_category_id;
        $product_category = ProductCategory::query()->where('id', $product_category_id)->first();
        $second_pr_category = $first_pr_category = null;
        if (isset($product_category)) {
            if ($product_category->parent_id != 0) {
                $second_pr_category = $product_category;
                $first_pr_category = ProductCategory::query()
                    ->where('id', $product_category->parent_id)
                    ->first();
            } else {
                $first_pr_category = $product_category;
            }
        }

        /*$product_images = \App\Models\ProductImage::query()
            ->where('product_id', $product->id)
            ->orderby('order', 'asc')
            ->limit(3)
            ->get();*/

        return view('frontend.product.detail', compact(
            'product',
            'product_it',
            'product_prices',
            'product_promotions',
            'locations',
            'first_pr_category',
            'second_pr_category',
            'product_same_prices',
            'product_relations'
            //'product_images'
        ));
    }

    public function search()
    {
        $q = request()->get('q');
        $products = \App\Models\Product::query()
            ->where('name', 'like', '%' . $q . '%')
            ->orderBy('price', 'desc')
            ->paginate(100);
        return view('frontend.product.search', compact(
            'products'
        ));
    }
}