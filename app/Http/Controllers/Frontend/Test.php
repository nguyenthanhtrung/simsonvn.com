<?php
namespace App\Http\Controllers\Frontend;


use App\Jobs\SendEmailRequireActive;

use Queue;

class Test extends Base
{
    public function index() {
	    $data = [
		    'email'=>"trungntvn@gmail.com",
		    'timeout'=>100
	    ];
	    Queue::connection('database')
		    ->push(new SendEmailRequireActive($data));
	    echo 'done';
    }
}