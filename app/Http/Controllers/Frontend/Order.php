<?php
namespace App\Http\Controllers\Frontend;


use App\Common\AjaxResponse;
use App\Models\Product;


class Order extends Base
{
    public function create() {
        $name = request()->get('name','');
	    $description = request()->get('description','');
	    $phone = request()->get('phone','');
	    $address = request()->get('address','');
	    $product_id = request()->get('product_id',0);
	    if($name == ''  || $phone == '') {
		    return AjaxResponse::responseError('Vui lòng kiểm tra thông tin vừa nhập');
	    }
	    $product = Product::query()->where('id', $product_id)->first();
	    $product_image = $product->image;
	    $product_name = $product->name;
	    $data = [
		    'product_id'=>$product_id,
		    'product_name'=>$product_name,
		    'product_image'=>$product_image,
		    'name'=>$name,
		    'description'=>$description,
		    'phone'=>$phone,
		    'address'=>$address,
		    'status'=>'PENDING'
	    ];
		$order = new \App\Models\Order($data);
		$order->save();

	    return AjaxResponse::responseSuccess('success');
    }
}