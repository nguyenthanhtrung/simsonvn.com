<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Partner;
use App\Models\ProductCategory;
use App\Models\Video;

/**
 * Class Home
 * @package App\Http\Controllers\Frontend
 */
class Home extends Base
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /*$product_categories = ProductCategory::query()
            ->with(['categories'])
            ->where('parent_id', 0)
            ->where('is_home', 1)
            ->orderBy('order', 'asc')
            ->get();

        foreach ($product_categories as $product_category) {
            $product_category_ids = array_pluck($product_category->categories->toArray(), 'id');

            $product_category_ids[] = $product_category->id;

            $product_category->products = \App\Models\Product::query()
                ->whereIn('product_category_id', $product_category_ids)
                ->where('display_in_home', 1)
                ->orderBy('created_time', 'desc')
                ->limit(8)
                ->get();
        }*/
        $product_categories = ProductCategory::query()
            ->with(['categories'])
	        ->where('parent_id',0)
            //->where('is_home', 1)
            ->orderBy('order', 'asc')
	        ->take(8)->offset(0)
            ->get();

        $video = Video::query()->orderBy('order', 'asc')->first();

        $posts = \App\Models\Post::query()
            ->where('status', 1)
            ->where('homepage', 1)
            ->orderBy('order', 'asc')
            ->limit(10)
            ->get();

        $partners = Partner::query()->where('status', 1)->orderBy('order', 'asc')->get();

        return view('frontend.home.index', compact(
            'product_categories',
            'video',
            'posts',
            'partners'
        ));
    }
}