<?php
namespace App\Http\Controllers\Frontend;

use App\Models\PostCategory;
use App\Models\Post as PostModel;

class Post extends Base
{
    public function index($post_category_slug) {

	    if($post_category_slug == 'gioi-thieu') {
		    return $this->show('', 1);
	    }

	    if($post_category_slug == 'lien-he') {
		    return $this->show('', 2);
	    }
		$post_category = PostCategory::query()
			->where('slug',$post_category_slug)->first();
	    if(!isset($post_category)) {
		    abort(404);
	    }

	    $posts = \App\Models\Post::query()->where('category_id', $post_category->id)->get();
	    return view('frontend.post.index', compact('posts', 'post_category'));
    }

    public function show($slug, $id) {

	    $post = PostModel::query()
		    ->where('id', $id)
		    ->first();

	    if (!$post instanceof PostModel) {
		    abort(404);
	    }
	    $post_category = PostCategory::query()
		    ->where('id', $post->category_id)
		    ->first();


	    return view('frontend.post.show', compact('post', 'post_category'));
    }
}