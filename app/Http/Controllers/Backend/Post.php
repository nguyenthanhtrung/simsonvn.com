<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Post extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách Bài viết',
            'items' => [
                [
                    'title' => 'Danh sách Bài viết',
                    'url' => '/backend/post'
                ]
            ]
        ];

        $posts = \App\Models\Post::query()->orderBy('created_time', 'desc')
            ->orderBy('created_time', 'desc')->paginate(24);
        return view('backend.post.index', compact('posts', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post_categories = \App\Models\Helper\PostCategory::getList();

        $breadcrumb = [
            'title' => 'Thêm Bài viết',
            'items' => [
                [
                    'title' => 'Danh sách Bài viết',
                    'url' => '/backend/post'
                ],
                [
                    'title' => 'Thêm Bài viết'
                ]
            ]
        ];

        return view('backend.post.create', compact(
            'post_categories',
            'breadcrumb'
        ));
    }


    public function store(Request $request)
    {

        $user = auth()->user();

        $post = new \App\Models\Post();
        $title = $request->get('title','');
        $slug = strtolower(Utility::makeSlug($title));
        $introduce = $request->get('introduce','');
        $content = $request->get('content','');
        $category_id = $request->get('category_id','');
        $meta_description = $request->get('meta_description','');
        $meta_title = $request->get('meta_title','');
        $meta_keywords = $request->get('meta_keywords','');

        $order = $request->get('order','');
        $status = $request->get('status','');

        if($status == 'on') {
            $status = 1;
        }else{
            $status = 0;
        }
        $post->title = $title;
        $post->slug = $slug;
        $post->introduce = $introduce;
        $post->content = $content;
        $post->category_id = $category_id;
        $post->meta_title = $meta_description;
        $post->meta_description = $meta_title;
        $post->meta_keywords = $meta_keywords;
        $post->created_by_id = $user->id;
        $post->created_by_name = $user->username;
        $post->updated_by_id = $user->id;
        $post->updated_by_name = $user->username;
        $post->status = $status;
        $post->homepage = 1;
        $post->order = $order;

        if ($post->save()) {
            if (Input::hasFile('thumb')) {
                $image = Input::file('thumb');

                $file = ['image' => $image];
                $validator = Validator::make($file, [
                    'image' => 'mimes:jpeg,bmp,png|max:1000'
                ]);
                if ($validator->fails()) {
                    return redirect()->action('Backend\Post@edit', ['id' => $post->id])->withInput()->withErrors($validator);
                }

                $fileName = $post->id . '.jpg';

                $filePath = env('UPLOAD_PATH').'/post/';

                $image->move($filePath, $fileName);

                $post->image = env('WEB_PATH').'/upload/post/'.$fileName;;
                $post->save();
            }

            return redirect('backend/post')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/post')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = \App\Models\Post::findOrFail($id);

        $post_categories = \App\Models\Helper\PostCategory::getList();

        $breadcrumb = [
            'title' => $post->title,
            'items' => [
                [
                    'title' => 'Danh sách Bài viết',
                    'url' => '/backend/post'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('post.show', ['id' => $post])
                ],
            ]
        ];

        return view('backend.post.edit', compact(
            'post',
            'post_categories',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $user = auth()->user();

        $title = $request->get('title','');
        $slug = strtolower(Utility::makeSlug($title));
        $introduce = $request->get('introduce','');
        $content = $request->get('content','');
        $category_id = $request->get('category_id','');
        $meta_description = $request->get('meta_description','');
        $meta_title = $request->get('meta_title','');
        $meta_keywords = $request->get('meta_keywords','');
        $homepage = $request->get('homepage','');
        $order = $request->get('order','');
        $status = $request->get('status','');
        if($status == 'on') {
            $status = 1;
        }else{
            $status = 0;
        }
        $post = \App\Models\Post::findOrFail($id);
        $post->title = $title;
        $post->slug = $slug;
        $post->introduce = $introduce;
        $post->content = $content;
        $post->category_id = $category_id;
        $post->meta_title = $meta_description;
        $post->meta_description = $meta_title;
        $post->meta_keywords = $meta_keywords;

        $post->updated_by_id = $user->id;
        $post->updated_by_name = $user->username;
        $post->status = $status;
        $post->homepage = 1;
        $post->order = $order;
        
        if ($post->save()) {
            if (Input::hasFile('thumb')) {
                $image = Input::file('thumb');

                $file = ['image' => $image];
                $validator = Validator::make($file, [
                    'image' => 'mimes:jpeg,bmp,png|max:1000'
                ]);
                if ($validator->fails()) {
                    return redirect()->action('Backend\Post@edit', ['id' => $post->id])->withInput()->withErrors($validator);
                }

                $fileName = $post->id . '.jpg';

                $filePath = env('UPLOAD_PATH').'/post/';

                $image->move($filePath, $fileName);

                $post->image = env('WEB_PATH').'/upload/post/'.$fileName;;
                $post->save();
            }

            return redirect('backend/post')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/post')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = \App\Models\Post::findOrFail($id);

        if ($post->delete()) {
            session()->flash('success', 'Xóa thành công Bài viết #' . $id);

            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công Bài viết #' . $id,
            ]);
        } else {
            session()->flash('error', 'Không thể xóa Bài viết #' . $id);

            return response()->json([
                'error' => 1,
                'message' => 'Không thể xóa Bài viết #' . $id,
            ]);
        }
    }

    public function updateHomePage(Request $request)
    {
        $id = $request->get('id', '');
        $is_check = $request->get('is_check', 0);

        if (!empty($id)) {
            $post = \App\Models\Post::query()->where('id', $id)->first();

            if ($post instanceof \App\Models\Post) {
                $post->homepage = $is_check;

                $post->save();
            }
        }
    }

    public function updateOrder(Request $request)
    {
        $id = $request->get('id', '');
        $value = $request->get('value', 0);

        if (!empty($id)) {
            $post = \App\Models\Post::query()->where('id', $id)->first();

            if ($post instanceof \App\Models\Post) {
                $post->order = intval($value);

                $post->save();
            }
        }
    }

}