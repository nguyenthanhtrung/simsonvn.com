<?php

namespace App\Http\Controllers\Backend;

use App\Commons\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class BannerSub extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Banner nhỏ',
            'items' => [
                [
                    'title' => 'Danh sách banner nhỏ',
                    'url' => '/backend/banner_sub'
                ]
            ]
        ];

        $datas = \App\Models\BannerSub::query()->orderBy('order', 'asc')->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.banner_sub.index', compact('datas', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm video',
            'items' => [
                [
                    'title' => 'Danh sách banner nhỏ',
                    'url' => '/backend/banner_sub'
                ],
                [
                    'title' => 'Thêm banner nhỏ'
                ]
            ]
        ];

        return view('backend.banner_sub.create', compact(
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$name = $request->get('name', '');
		$link = $request->get('link', '');
	    $order = $request->get('order', 0);
        $data = [
	        'name'=>$name,
	        'link'=>$link,
	        'order'=>$order
        ];
	    $odm = new \App\Models\BannerSub($data);
	    if ($odm->save()) {
		    if (request()->hasFile('image')) {
			    $image = request()->file('image');
			    $fileName = 'bs'.$odm->id.'.jpg';
			    $filePath = env('UPLOAD_PATH').'banner_sub/';
			    $url_file_update = env('WEB_PATH').'upload/banner_sub/'.$fileName.'?v='.time();

			    if(!is_dir($filePath)) {
				    \App\Common\Utility::makeDirectory($filePath);
			    }
			    $image->move($filePath, $fileName);

			    $data_update = [
				    'image_url'=>$url_file_update
			    ];
			    $odm->update($data_update);
		    }

		    return redirect()->action('Backend\BannerSub@index')->with('success','Thêm mới banner nhỏ thành công.');
	    }



        return redirect('backend/banner_sub')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $odm = \App\Models\BannerSub::findOrFail($id);

        $breadcrumb = [
            'title' => $odm->name,
            'items' => [
                [
                    'title' => 'Danh sách banner nhỏ',
                    'url' => '/backend/banner_sub'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => ''
                ]
            ]
        ];

        return view('backend.banner_sub.show', compact(
            'odm',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $odm = \App\Models\BannerSub::findOrFail($id);

        $breadcrumb = [
            'title' => $odm->title,
            'items' => [
                [
                    'title' => 'Danh sách banner nhỏ',
                    'url' => '/backend/banner_sub'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('banner_sub.show', ['id' => $odm])
                ],
            ]
        ];

        return view('backend.banner_sub.edit', compact(
            'odm',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $odm = \App\Models\BannerSub::query()->where('id',$id)->first();

	    $name = $request->get('name', '');
	    $link = $request->get('link', '');
	    $order = $request->get('order', 0);
	    $data = [
		    'name'=>$name,
		    'link'=>$link,
		    'order'=>$order
	    ];


        if ($odm->update($data)) {
	        if (request()->hasFile('image')) {


		        $image = request()->file('image');
		        $fileName = 'bs'.$odm->id.'.jpg';
		        $filePath = env('UPLOAD_PATH').'banner_sub/';
		        //neu co thi xoa di
		        $exist_file = $filePath.$fileName;
		        if(is_file($exist_file)) {
			        unset($exist_file);
		        }
				//
		        $url_file_update = env('WEB_PATH').'upload/banner_sub/'.$fileName.'?v='.time();

		        if(!is_dir($filePath)) {
			        \App\Common\Utility::makeDirectory($filePath);
		        }
		        $image->move($filePath, $fileName);

		        $data_update = [
			        'image_url'=>$url_file_update
		        ];
		        $odm->update($data_update);
	        }

            return redirect()->route('banner_sub.edit', ['id' => $id])->with('success','Cập nhật thành công');
        }

        return redirect('backend/banner_sub')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {}

    public function delete($id)
    {
        \App\Models\BannerSub::query()->where('id',$id)->delete();
        return redirect('backend/banner_sub')->with('success', 'Xóa thành công');
        
    }

}