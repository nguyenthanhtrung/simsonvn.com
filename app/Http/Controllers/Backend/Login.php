<?php

namespace App\Http\Controllers\Backend;

use App\Models\Admin;

class Login extends Base
{
    
    public function index() {
        return view('backend.login');
    }
    
    public function login()
    {
        $username = request('username','');
        $password = request('password','');
        $exist = Admin::query()
            ->where('username', $username)
            ->where('password',md5($password))->first();
        if(isset($exist) && $exist instanceof Admin) {
            /**@var \Illuminate\Contracts\Auth\Authenticatable $exist*/
            auth()->login($exist);

            return redirect(action('Backend\Home@index'));

        }
        return back()->withErrors('Invalid account!');
    }


    public function logout() {
        auth()->logout();
        return redirect()->to(url('backend/login'));
    }
}
