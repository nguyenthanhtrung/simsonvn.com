<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;


class ProductPromotion extends Base
{

    public function lists() {
        $product_id = request()->get('id');
        $product_promotions = \App\Models\ProductPromotion::query()
            ->where('product_id', $product_id)
            ->orderBy('order','asc')
            ->get();
        $html = view('backend.product.ajax.list_promotion', compact(
            'product_promotions'
        ))->render();
        return AjaxResponse::responseSuccess('success',[
            'html'=>$html
        ]);
    }

    public function add() {
        $product_id = request()->get('id');
        $promotion = request()->get('promotion');
        $order = request()->get('order');
        
        $data = [
            'product_id'=>$product_id,
            'content'=>$promotion,
            'order'=>$order
        ];
        $odm = new \App\Models\ProductPromotion($data);
        $odm->save();
        return AjaxResponse::responseSuccess('success');
    }

    public function remove() {
        $id = request()->get('id');
        \App\Models\ProductPromotion::query()->where('id',$id)->delete();
        return AjaxResponse::responseSuccess('success');
    }
}