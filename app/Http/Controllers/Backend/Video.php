<?php

namespace App\Http\Controllers\Backend;

use App\Commons\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Video extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách Video',
            'items' => [
                [
                    'title' => 'Danh sách Video',
                    'url' => '/backend/video'
                ]
            ]
        ];

        $datas = \App\Models\Video::query()->orderBy('order', 'asc')->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.video.index', compact('datas', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm video',
            'items' => [
                [
                    'title' => 'Danh sách Video',
                    'url' => '/backend/video'
                ],
                [
                    'title' => 'Thêm video'
                ]
            ]
        ];

        return view('backend.video.create', compact(
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $odm = new \App\Models\Video();

        $odm->name = $request->get('name', '');
        $odm->embed_code = $request->get('embed_code', '');
        $odm->order = $request->get('order', 0);
        $odm->position = $request->get('position', 0);
        
        if ($odm->save()) {
            return redirect()->action('Backend\Video@index');
        }

        return redirect('backend/video')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $odm = \App\Models\Video::findOrFail($id);

        $breadcrumb = [
            'title' => $odm->name,
            'items' => [
                [
                    'title' => 'Danh sách Video',
                    'url' => '/backend/video'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => ''
                ]
            ]
        ];

        return view('backend.video.show', compact(
            'odm',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $odm = \App\Models\Video::findOrFail($id);

        $breadcrumb = [
            'title' => $odm->title,
            'items' => [
                [
                    'title' => 'Danh sách Video',
                    'url' => '/backend/video'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('video.show', ['id' => $odm])
                ],
            ]
        ];

        return view('backend.video.edit', compact(
            'odm',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $odm = \App\Models\Video::findOrFail($id);

        $odm->name = $request->get('name', '');
        $odm->embed_code = $request->get('embed_code', '');
        $odm->description = $request->get('description', '');
        $odm->order = $request->get('order', 0);
        $odm->position = $request->get('position', 0);

        if ($odm->save()) {
            return redirect()->route('video.edit', ['id' => $id]);
        }

        return redirect('backend/video')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {}

    public function delete($id)
    {
        \App\Models\Video::query()->where('id',$id)->delete();
        return redirect('backend/video')->with('success', 'Xóa thành công');
        
    }

}