<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;


class ProductPrice extends Base
{

    public function lists() {
        $product_id = request()->get('id');
        $product_prices = \App\Models\ProductPrice::query()
            ->where('product_id', $product_id)
            ->orderBy('order','asc')
            ->get();
        $html = view('backend.product.ajax.list_price', compact(
            'product_prices'
        ))->render();
        return AjaxResponse::responseSuccess('success',[
            'html'=>$html
        ]);
    }

    public function add() {
        $product_id = request()->get('id');
        $color = request()->get('color');
        $price = request()->get('price');
        $order = request()->get('order');
        
        $price = str_replace(',','',$price);
        $data = [
            'product_id'=>$product_id,
            'color'=>$color,
            'price'=>$price,
            'order'=>$order
        ];
        $odm = new \App\Models\ProductPrice($data);
        $odm->save();
        return AjaxResponse::responseSuccess('success');
    }

    public function remove() {
        $id = request()->get('id');
        \App\Models\ProductPrice::query()->where('id',$id)->delete();
        return AjaxResponse::responseSuccess('success');
    }
}