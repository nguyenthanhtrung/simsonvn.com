<?php
namespace App\Http\Controllers\Backend;


class Home extends Base
{
    public function index() {
        
        return view('backend.home.index');
    }
}