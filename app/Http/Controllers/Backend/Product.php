<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;
use App\Common\Utility;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductIt;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Product extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách sản phẩm',
            'items' => [

            ]
        ];

        $datas = \App\Models\Product::query()->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.product.index', compact('datas', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm sản phẩm',
            'items' => [
                [
                    'title' => 'Danh sách sản phẩm',
                    'url' => '/backend/product'
                ],
                [
                    'title' => 'Thêm sản phẩm'
                ]
            ]
        ];
        $product_categories = ProductCategory::query()->get();
        //$product_brands = ProductBrand::query()->get();
        return view('backend.product.create', compact(
            'breadcrumb',
            'product_categories'
            //'product_brands'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');
        $product_category = $product_brand = null;
        $product_category_id = $request->get('product_category',0);
        if(isset($product_category_id) && $product_category_id > 0) {
            $product_category = ProductCategory::query()->where('id',$product_category_id)->first();
        }

        /*$product_brand_id = $request->get('product_brand');
        if(isset($product_brand_id) && $product_brand_id > 0) {
            $product_brand = ProductBrand::query()->where('id',$product_brand_id)->first();
        }*/
        $price = $request->get('price');
        $price = str_replace(',','',$price);

        $price_promotion = $request->get('price_promotion');
        $price_promotion = str_replace(',','',$price_promotion);


        $stock = $request->get('stock');
        $display_in_home = $request->get('display_in_home');
        $status = $request->get('status',0);
        $description = $request->get('description',0);

        $user = auth()->user();
        $data = [
            'name'=>$name,
            'slug'=>Utility::makeSlug($name),
            'product_category_id'=>$product_category->id,
            'product_category_slug'=>$product_category->slug,
            'product_category_name'=>$product_category->name,
            /*'product_brand_id'=>$product_brand->id,
            'product_brand_name'=>$product_brand->name,*/
            'price'=>$price,
            'price_promotion'=>$price_promotion,
            'stock'=>$stock,
            'display_in_home'=>$display_in_home,
            'status'=>$status,
            'description'=>$description,
            'created_by_id'=>$user->id,
            'created_by_name'=>$user->username,
            'updated_by_id'=>$user->id,
            'updated_by_name'=>$user->username,
        ];
        $odm = new \App\Models\Product($data);
        $insert = $odm->save();

        if ($insert) {

	        if (Input::hasFile('image')) {
		        $image = Input::file('image');
		        $fileName = 'b'.$odm->id.'.jpg';
		        $filePath = env('UPLOAD_PATH').'/product/';
		        $url_file_update = env('WEB_PATH').'upload/product/'.$fileName;

		        if(!is_dir($filePath)) {
			        Utility::makeDirectory($filePath);
		        }
		        $image->move($filePath, $fileName);

		        $data_update = [
			        'image'=>$url_file_update,
			        'image_url'=>$url_file_update,
			        'image_name'=>$fileName
		        ];
		        $odm->update($data_update);
	        }



            return redirect('backend/product')->with('success', 'Thêm sản phẩm thành công');
        }

        return redirect('backend/product')->withErrors('Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Models\Product::findOrFail($id);

        $breadcrumb = [
            'title' => $user->title,
            'items' => [
                [
                    'title' => 'Chi tiết sản phẩm',
                    'url' => '/backend/product'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('product_category.edit', ['id' => $user])
                ]
            ]
        ];

        return view('backend.product.show', compact(
            'user',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = \App\Models\Product::findOrFail($id);

        $breadcrumb = [
            'title' => 'Chỉnh sửa thông tin sản phẩm "'.$data->name.'"',
            'items' => [
                [
                    'title' => 'Danh sách sản phẩm',
                    'url' => '/backend/product'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('product.show', ['id' => $data])
                ],
            ]
        ];
        $product_categories = ProductCategory::query()->get();

        return view('backend.product.edit', compact(
            'data',
            'breadcrumb',
            'product_categories'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $odm = \App\Models\Product::query()->where('id',$id)->first();
        $name = $request->get('name');
        $product_category = $product_brand = null;
        $product_category_id = $request->get('product_category',0);
        if(isset($product_category_id) && $product_category_id > 0) {
            $product_category = ProductCategory::query()->where('id',$product_category_id)->first();
        }

        /*$product_brand_id = $request->get('product_brand');
        if(isset($product_brand_id) && $product_brand_id > 0) {
            $product_brand = ProductBrand::query()->where('id',$product_brand_id)->first();
        }*/
        $price = $request->get('price');
        $price = str_replace(',','',$price);

        $price_promotion = $request->get('price_promotion');
        $price_promotion = str_replace(',','',$price_promotion);

        $stock = $request->get('stock');
        $display_in_home = $request->get('display_in_home');
        $status = $request->get('status',0);
        $description = $request->get('description',0);


        $data = [
            'name'=>$name,
            'product_category_id'=>$product_category->id,
            'product_category_name'=>$product_category->name,
            /*'product_brand_id'=>$product_brand->id,
            'product_brand_name'=>$product_brand->name,*/
            'price'=>$price,
            'price_promotion'=>$price_promotion,
            'stock'=>$stock,
            'display_in_home'=>$display_in_home,
            'status'=>$status,
            'description'=>$description
        ];

        $update = $odm->update($data);

        if ($update) {
	        if (Input::hasFile('image')) {
		        $image = Input::file('image');
		        $fileName = 'b'.$odm->id.'.jpg';
		        $filePath = env('UPLOAD_PATH').'/product/';
		        $url_file_update = env('WEB_PATH').'upload/product/'.$fileName;

		        if(!is_dir($filePath)) {
			        Utility::makeDirectory($filePath);
		        }
		        $image->move($filePath, $fileName);

		        $data_update = [
			        'image'=>$url_file_update.'?v='.time(),
			        'image_url'=>$url_file_update.'?v='.time(),
			        'image_name'=>$fileName
		        ];
		        $odm->update($data_update);
	        }
            return redirect('backend/product')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/product')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}
    
    public function delete($id)
    {
        \App\Models\Product::query()->where('id',$id)->delete();
        return redirect('backend/product')->with('success', 'Xóa thành công');
    }



}