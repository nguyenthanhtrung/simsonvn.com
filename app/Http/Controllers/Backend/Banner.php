<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Banner extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách Banner',
            'items' => [
                [
                    'title' => 'Danh sách Banner',
                    'url' => '/backend/banner'
                ]
            ]
        ];

        $banners = \App\Models\Banner::query()->orderBy('order', 'asc')->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.banner.index', compact('banners', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm Banner',
            'items' => [
                [
                    'title' => 'Danh sách Banner',
                    'url' => '/backend/banner'
                ],
                [
                    'title' => 'Thêm Banner'
                ]
            ]
        ];

        return view('backend.banner.create', compact(
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name', '');
        $description = $request->get('description', '');
        $order = $request->get('order', '');
        $data = [
            'name'=>$name,
            'description'=>$description,
            'order'=>$order,
        ];
        $banner = new \App\Models\Banner($data);
        $update = $banner->save($data);
        if ($update) {
            if (Input::hasFile('image')) {
                $image = Input::file('image');
                $fileName = 'b'.$banner->id.'.jpg';
                $filePath = env('UPLOAD_PATH').'/banner/';
                $url_file_update = env('WEB_PATH').'upload/banner/'.$fileName;

                if(!is_dir($filePath)) {
                    Utility::makeDirectory($filePath);
                }
                $image->move($filePath, $fileName);

                $data_update = [
                    'image_url'=>$url_file_update,
                    'image_name'=>$fileName
                ];
                $banner->update($data_update);
            }

            return redirect()->action('Backend\Banner@index');
        }

        return redirect('backend/banner')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = \App\Models\Banner::findOrFail($id);

        $breadcrumb = [
            'title' => $banner->name,
            'items' => [
                [
                    'title' => 'Danh sách Banner',
                    'url' => '/backend/banner'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('banner.edit', ['id' => $banner])
                ]
            ]
        ];

        return view('backend.banner.show', compact(
            'banner',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = \App\Models\Banner::findOrFail($id);

        $breadcrumb = [
            'title' => $banner->title,
            'items' => [
                [
                    'title' => 'Danh sách Banner',
                    'url' => '/backend/banner'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('banner.show', ['id' => $banner])
                ],
            ]
        ];

        return view('backend.banner.edit', compact(
            'banner',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Input::hasFile('image')) {
            $image = Input::file('image');

            $file = ['image' => $image];
            $validator = Validator::make($file, [
                'image' => 'mimes:jpeg,bmp,png|max:1000'
            ]);

            if ($validator->fails()) {
                return redirect()->action('Backend\Banner@create')->withInput()->withErrors($validator);
            }
        }

        $banner = \App\Models\Banner::query()->where('id',$id)->first();

        $banner->name = $request->get('name', '');
        $banner->description = $request->get('description', '');
        $banner->order = $request->get('order', 0);

        if ($banner->save()) {
            if (Input::hasFile('image')) {
                $image = Input::file('image');
                $fileName = 'b'.$banner->id.'.jpg';
                $filePath = env('UPLOAD_PATH').'/banner/';
                $url_file_update = env('WEB_PATH').'upload/banner/'.$fileName;

                if(!is_dir($filePath)) {
                    Utility::makeDirectory($filePath);
                }
                $image->move($filePath, $fileName);

                $data_update = [
                    'image_url'=>$url_file_update,
                    'image_name'=>$fileName
                ];
                $banner->update($data_update);
            }

            return redirect()->action('Backend\Banner@show', ['id' => $id]);
        }

        return redirect('backend/banner')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function delete($id) {
        $banner =  \App\Models\Banner::query()->where('id',$id)->first();

        $image_path = env('UPLOAD_PATH').'/banner/'.$banner->name;
        if(is_file($image_path)) {
            unlink($image_path);
        }

        $banner->delete();
        return redirect()->action('Backend\Banner@index');
    }
}