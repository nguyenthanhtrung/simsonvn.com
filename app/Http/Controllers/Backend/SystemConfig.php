<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SystemConfig extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $breadcrumb = [
            'title' => 'Danh sách cấu hình',
            'items' => [
                [
                    'title' => 'Danh sách cấu hình',
                    'url' => '/backend/system_config'
                ],
                [
                    'title' => 'Danh sách'
                ]
            ]
        ];

        $system_configs = \App\Models\SystemConfig::query()->orderBy('key')->paginate(24);

        return view('backend.system_config.index', compact('system_configs', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm Cấu hình',
            'items' => [
                [
                    'title' => 'Danh sách cấu hình',
                    'url' => '/backend/system_config'
                ],
                [
                    'title' => 'Thêm Cấu hình'
                ]
            ]
        ];

        return view('backend.system_config.create', compact(
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $system_config = new \App\Models\SystemConfig();

        $system_config->key = $request->get('key', '');
        $system_config->value = $request->get('value', '');
        $system_config->description = $request->get('description', '');
        
        if ($system_config->save()) {
            return redirect('backend/system_config')->with('success', 'Thêm Danh mục thành công');
        }

        return redirect('backend/system_config')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $system_config = \App\Models\SystemConfig::findOrFail($id);

        $breadcrumb = [
            'title' => $system_config->title,
            'items' => [
                [
                    'title' => 'Danh sách cấu hình',
                    'url' => '/backend/system_config'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('system_config.edit', ['id' => $id])
                ]
            ]
        ];

        return view('backend.system_config.show', compact(
            'system_config',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $system_config = \App\Models\SystemConfig::findOrFail($id);

        $breadcrumb = [
            'title' => $system_config->username,
            'items' => [
                [
                    'title' => 'Danh sách cấu hình',
                    'url' => '/backend/system_config'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('system_config.show', ['id' => $id])
                ],
            ]
        ];

        return view('backend.system_config.edit', compact(
            'system_config',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $__request = $request->all();

        $system_config = \App\Models\SystemConfig::findOrFail($id);

        $system_config->key = $request->get('key', '');
        $system_config->value = $request->get('value', '');
        $system_config->description = $request->get('description', '');

        if ($system_config->save()) {
            return redirect('backend/system_config')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/system_config')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $system_config = \App\Models\SystemConfig::findOrFail($id);

        if ($system_config->delete()) {
            session()->flash('success', 'Xóa thành công #' . $id);

            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công #' . $id,
            ]);
        } else {
            session()->flash('error', 'Không thể xóa#' . $id);

            return response()->json([
                'error' => 1,
                'message' => 'Không thể xóa#' . $id,
            ]);
        }
    }
}