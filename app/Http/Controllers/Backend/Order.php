<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;
use App\Commons\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Order extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách đơn hàng',
            'items' => [
                [
                    'title' => 'Danh sách đơn hàng',
                    'url' => '/backend/order'
                ]
            ]
        ];

        $orders = \App\Models\Order::query()->orderBy('id','desc')->paginate(20);

        return view('backend.order.index', compact('orders', 'breadcrumb'));
    }

	public function changeStatus(){
		$id = request()->get('id');
		$status = request()->get('status');
		\App\Models\Order::query()->where('id',$id)->update([
			'status'=>$status
		]);

		return AjaxResponse::responseSuccess('success');
	}

	public function delete() {
		$id = request()->get('id');
		\App\Models\Order::query()->where('id', $id)->delete();

		return redirect()->action('Backend\Order@index')->with('success','Xóa đơn hàng thành công');

	}

}