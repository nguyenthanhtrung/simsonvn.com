<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProductCategory extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách danh mục sản phẩm',
            'items' => [
                [
                    'title' => 'Danh sách danh mục sản phẩm',
                    'url' => '/backend/product_category'
                ]
            ]
        ];

        $datas = \App\Models\ProductCategory::query()
            ->orderBy('created_time', 'desc')
            ->where('parent_id',0)
            ->paginate(24);
        
        return view('backend.product_category.index', compact('datas', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm danh mục sản phẩm',
            'items' => [
                [
                    'title' => 'Danh sách danh mục sản phẩm',
                    'url' => '/backend/product_category'
                ],
                [
                    'title' => 'Thêm danh mục sản phẩm'
                ]
            ]
        ];
        $parents = \App\Models\ProductCategory::query()->get();
        return view('backend.product_category.create', compact(
            'breadcrumb',
            'parents'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');
        $order = $request->get('order');
        $description = $request->get('description');
        $parent_id = request()->get('parent_id',0);

        $data = [
            'name'=>$name,
            'slug'=>Utility::makeSlug($name),
            'parent_id'=>$parent_id,
            'order'=>$order,
            'description'=>$description
        ];
        $odm = new \App\Models\ProductCategory($data);
        $insert = $odm->save();

        if ($insert) {

	        if (Input::hasFile('image')) {
		        $image = Input::file('image');
		        $fileName = 'pc'.$odm->id.'.jpg';
		        $filePath = env('UPLOAD_PATH').'/product_category/';
		        $url_file_update = env('WEB_PATH').'upload/product_category/'.$fileName;

		        if(!is_dir($filePath)) {
			        Utility::makeDirectory($filePath);
		        }
		        $image->move($filePath, $fileName);

		        $data_update = [
			        'image_url'=>$url_file_update,
			        'image_name'=>$fileName
		        ];
		        $odm->update($data_update);
	        }

            return redirect('backend/product_category')->with('success', 'Thêm Danh mục thành công');
        }

        return redirect('backend/product_category')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Models\ProductCategory::findOrFail($id);

        $breadcrumb = [
            'title' => $user->title,
            'items' => [
                [
                    'title' => 'Danh sách danh mục sản phẩm',
                    'url' => '/backend/product_category'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('product_category.edit', ['id' => $user])
                ]
            ]
        ];

        return view('backend.product_category.show', compact(
            'user',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = \App\Models\ProductCategory::findOrFail($id);

        $breadcrumb = [
            'title' => $data->name,
            'items' => [
                [
                    'title' => 'Danh sách danh mục sản phẩm',
                    'url' => '/backend/product_category'
                ],
                [
                    'title' => 'Sửa danh mục',
                    'url' => ''
                ],
            ]
        ];
        $parents = \App\Models\ProductCategory::query()->where('id','!=',$id)->get();
        return view('backend.product_category.edit', compact(
            'data',
            'parents',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $order = $request->get('order');
        $description = $request->get('description');
        $parent_id = request()->get('parent_id',0);

        $data = [
            'name'=>$name,
            'slug'=>Utility::makeSlug($name),
            'parent_id'=>$parent_id,
            'order'=>$order,
            'description'=>$description
        ];
        $odm = \App\Models\ProductCategory::query()->where('id',$id)->first();
        $update = $odm->update($data);
        if ($update) {

	        if (Input::hasFile('image')) {
		        $image = Input::file('image');
		        $fileName = 'pc'.$id.'.jpg';
		        $filePath = env('UPLOAD_PATH').'/product_category/';
		        $url_file_update = env('WEB_PATH').'upload/product_category/'.$fileName;

		        if(!is_dir($filePath)) {
			        Utility::makeDirectory($filePath);
		        }
		        $image->move($filePath, $fileName);

		        $data_update = [
			        'image_url'=>$url_file_update,
			        'image_name'=>$fileName
		        ];
		        $odm->update($data_update);
	        }
            return redirect('backend/product_category')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/product_category')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function delete($id)
    {
        \App\Models\ProductCategory::query()->where('id',$id)->delete();
        return redirect('backend/product_category')->with('success', 'Xóa thành công');
    }
}