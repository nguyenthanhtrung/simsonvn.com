<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use Illuminate\Http\Request;

class ProductBrand extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách thương hiệu sản phẩm',
            'items' => [
                [
                    'title' => 'Danh sách thương hiệu sản phẩm',
                    'url' => '/backend/product_brand'
                ]
            ]
        ];

        $datas = \App\Models\ProductBrand::query()->orderBy('created_time', 'desc')->paginate(24);
        
        return view('backend.product_brand.index', compact('datas', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm thương hiệu sản phẩm',
            'items' => [
                [
                    'title' => 'Danh sách thương hiệu sản phẩm',
                    'url' => '/backend/product_brand'
                ],
                [
                    'title' => 'Thêm thương hiệu sản phẩm'
                ]
            ]
        ];
        $parents = \App\Models\ProductBrand::query()->get();
        return view('backend.product_brand.create', compact(
            'breadcrumb',
            'parents'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');


        $data = [
            'name'=>$name,
        ];
        $odm = new \App\Models\ProductBrand($data);
        $insert = $odm->save();

        if ($insert) {
            return redirect('backend/product_brand')->with('success', 'Thêm Danh mục thành công');
        }

        return redirect('backend/product_brand')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Models\ProductBrand::findOrFail($id);

        $breadcrumb = [
            'title' => $user->title,
            'items' => [
                [
                    'title' => 'Danh sách thương hiệu sản phẩm',
                    'url' => '/backend/product_brand'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('product_category.edit', ['id' => $user])
                ]
            ]
        ];

        return view('backend.product_brand.show', compact(
            'user',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = \App\Models\ProductBrand::findOrFail($id);

        $breadcrumb = [
            'title' => 'Chỉnh sửa thông tin thương hiệu'.$data->name,
            'items' => [
                [
                    'title' => 'Danh sách thương hiệu sản phẩm',
                    'url' => '/backend/product_brand'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('admin.show', ['id' => $data])
                ],
            ]
        ];

        return view('backend.product_brand.edit', compact(
            'data',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $odm = \App\Models\ProductBrand::findOrFail($id);
        $name = $request->get('name');


        $data = [
            'name'=>$name,
        ];
        $update = $odm->update($data);

        if ($update) {
            return redirect('backend/product_brand')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/product_brand')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function delete($id)
    {
        \App\Models\ProductBrand::query()->where('id',$id)->delete();
        return redirect('backend/product_brand')->with('success', 'Xóa thành công');
    }
}