<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;
use App\Common\Utility;
use App\Models\Product;
use Illuminate\Support\Facades\Input;

class ProductImage extends Base
{

    public function lists() {
        $product_id = request()->get('product_id');
        $product_images = \App\Models\ProductImage::query()
            ->where('product_id', $product_id)
            ->orderBy('order','desc')->get();
        $html = view('backend.product.ajax.list_images',compact('product_images'))->render();

        return AjaxResponse::responseSuccess('success',[
            'html'=>$html
        ]);
    }

    public function add() {

        $image = Input::file('file');
        $order = request()->get('order');
        $default = request()->get('default');
        $product_id = request()->get('product_id');

        $product = Product::query()->where('id', $product_id)->first();
        if(!isset($product)) {
            return AjaxResponse::responseError('Không tìm thấy sản phẩm');
        }
        $numberImage = \App\Models\ProductImage::query()
            ->where('product_id', $product_id)
            ->count();
        $numberImage = $numberImage+1;
        $numberImage = strlen($numberImage) == 2 ? $numberImage : '0'.$numberImage;

        $fileName = 'p'.$product->id.$numberImage.'.jpg';
        $filePath = env('UPLOAD_PATH').'product/';
        $url_file_update = env('WEB_PATH').'upload/product/'.$fileName;
        if(!is_dir($filePath)) {
            Utility::makeDirectory($filePath);
        }

        if($image->move($filePath, $fileName)) {

            $data = [
                'product_id'=>$product_id,
                'name'=>$fileName,
                'url'=>$url_file_update,
                'order'=>$order,
                'default'=>$default,
            ];
            $product_image = new \App\Models\ProductImage($data);
            $product_image->save();
            //
            if($default == 1) {
                $product->update([
                    'image'=>$url_file_update
                ]);
            }
            return AjaxResponse::responseSuccess('Cập nhật ảnh thành công');
        }
        return AjaxResponse::responseError('Không tìm thấy sản phẩm');
    }

    public function remove() {
        $id = request()->get('id');
        $product_image = \App\Models\ProductImage::query()->where('id', $id)->first();

        if(!isset($product_image)) {
            return AjaxResponse::responseError('Không tìm thấy ảnh để xóa');
        }
        $image_name = $product_image->name;
        $image_path = env('UPLOAD_PATH').'/product/'.$image_name;
        if(is_file($image_path)) {
            unlink($image_path);
        }
        $product_image->delete();
        //

        return AjaxResponse::responseSuccess('success');
    }
}