<?php

namespace App\Http\Controllers\Backend;

use App\Common\AjaxResponse;


class ProductIt extends Base
{

    public function add() {

        $product_id = request()->get('product_id');
        $it_monitor = request()->get('it_monitor');
        $it_os = request()->get('it_os');
        $it_camera_front = request()->get('it_camera_front');
        $it_camera_back = request()->get('it_camera_back');
        $it_cpu = request()->get('it_cpu');
        $it_ram = request()->get('it_ram');
        $it_disk = request()->get('it_disk');
        $it_sim = request()->get('it_sim');
        $it_pin = request()->get('it_pin');
        $it_design = request()->get('it_design');


        $data = [
            'product_id'=>$product_id,
            'monitor'=>$it_monitor,
            'os'=>$it_os,
            'camera_front'=>$it_camera_front,
            'camera_back'=>$it_camera_back,
            'cpu'=>$it_cpu,
            'ram'=>$it_ram,
            'disk'=>$it_disk,
            'sim'=>$it_sim,
            'pin'=>$it_pin,
            'design'=>$it_design,
        ];

        $product_it =  \App\Models\ProductIt::query()
            ->where('product_id', $product_id)
            ->first();
        if(isset($product_it) && !empty($product_it)) {
            $product_it->update($data);
            return AjaxResponse::responseSuccess('success');
        }

        (new \App\Models\ProductIt($data))->save();

        return AjaxResponse::responseSuccess('success');
    }

}