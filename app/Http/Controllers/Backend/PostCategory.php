<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use Illuminate\Http\Request;

class PostCategory extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh mục Bài viết',
            'items' => [
                [
                    'title' => 'Danh mục bài viết',
                    'url' => '/backend/post_category'
                ],
            ]
        ];

        $post_categories = \App\Models\PostCategory::query()->orderBy('order', 'asc')->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.post_category.index', compact('post_categories', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post_categories = \App\Models\Helper\PostCategory::getList();

        $breadcrumb = [
            'title' => 'Thêm danh mục bài viết',
            'items' => [
                [
                    'title' => 'Danh mục bài viết',
                    'url' => '/backend/post_category'
                ],
                [
                    'title' => 'Thêm Danh mục'
                ]
            ]
        ];
        return view('backend.post_category.create', compact(
            'post_categories',
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new \App\Models\PostCategory();

        $order = $request->get('order', '');
        $status = $request->get('status', '');

        $post->title = $request->get('title');
        $post->slug = strtolower(Utility::makeSlug($request->get('title')));
        $post->parent_id = $request->get('parent_id', 0);
        $post->order = !empty($order) ? intval($order) : 0;
        $post->status = !empty($status) ? 1 : 0;

        if ($post->save()) {
            return redirect('backend/post_category')->with('success', 'Thêm Danh mục thành công');
        }

        return redirect('backend/post_category')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post_category = \App\Models\PostCategory::findOrFail($id);

        $breadcrumb = [
            'title' => $post_category->title,
            'items' => [
                [
                    'title' => 'Danh mục bài viết',
                    'url' => '/backend/post_category'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('category.edit', ['id' => $post_category])
                ]
            ]
        ];

        return view('backend.post_category.show', compact(
            'post_category',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post_category = \App\Models\PostCategory::findOrFail($id);

        $post_categories = \App\Models\Helper\PostCategory::getList($id);

        $breadcrumb = [
            'title' => $post_category->title,
            'items' => [
                [
                    'title' => 'Danh mục bài viết',
                    'url' => '/backend/post_category'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('post_category.show', ['id' => $post_category])
                ],
            ]
        ];

        return view('backend.post_category.edit', compact(
            'post_category',
            'post_categories',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = \App\Models\PostCategory::findOrFail($id);

        $order = $request->get('order', '');
        $status = $request->get('status', '');

        $post->title = $request->get('title');
        $post->slug = strtolower(Utility::makeSlug($request->get('title')));
        $post->parent_id = $request->get('parent_id', 0);
        $post->order = !empty($order) ? intval($order) : 0;
        $post->status = !empty($status) ? 1 : 0;

        if ($post->save()) {
            return redirect('backend/post_category')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/post_category')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = \App\Models\PostCategory::findOrFail($id);

        if ($post->delete()) {
            session()->flash('success', 'Xóa thành công #' . $id);

            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công #' . $id,
            ]);
        } else {
            session()->flash('error', 'Không thể xóa#' . $id);

            return response()->json([
                'error' => 1,
                'message' => 'Không thể xóa#' . $id,
            ]);
        }
    }
}