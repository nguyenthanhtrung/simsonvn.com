<?php

namespace App\Http\Controllers\Backend;

use App\Common\UploadedImage;
use Illuminate\Http\Request;

class Image extends Base
{
    public function editorUpload(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileName = uniqid();

            $type = $request->get('type', '');

            $uploadedImage = new UploadedImage($request->file('file'));
            $uploadedImage->setFileName($fileName);
            $uploadedImage->setPathDir($type);
            $uploadedImage->move();
            $url = env('WEB_PATH') . 'upload/' . $type . '/' . $fileName . '.jpg';
            echo $url;exit;
        }
        echo '';exit;
    }
}