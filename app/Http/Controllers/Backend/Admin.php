<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

class Admin extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách quản trị',
            'items' => [

            ]
        ];

        $users = \App\Models\Admin::query()->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.admin.index', compact('users', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm quản trị',
            'items' => [
                [
                    'title' => 'Danh sách quản trị',
                    'url' => '/backend/admin'
                ],
                [
                    'title' => 'Thêm quản trị'
                ]
            ]
        ];

        return view('backend.admin.create', compact(
            'breadcrumb'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $__request = $request->all();

        $user = new \App\Models\Admin();

        $user->username = $__request["username"];
        $user->password = md5($__request["password"]);

        $user->status = isset($__request["status"]) ? 1 : 0;

        $insert = $user->save();

        if ($insert) {
            return redirect('backend/user')->with('success', 'Thêm Danh mục thành công');
        }

        return redirect('backend/user')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Models\Admin::findOrFail($id);

        $breadcrumb = [
            'title' => $user->title,
            'items' => [
                [
                    'title' => 'Danh sách quản trị',
                    'url' => '/backend/admin'
                ],
                [
                    'title' => 'Sửa thông tin',
                    'url' => route('admin.edit', ['id' => $user])
                ]
            ]
        ];

        return view('backend.admin.show', compact(
            'user',
            'breadcrumb'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Models\Admin::findOrFail($id);

        $breadcrumb = [
            'title' => $user->username,
            'items' => [
                [
                    'title' => 'Danh sách quản trị',
                    'url' => '/backend/admin'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('admin.show', ['id' => $user])
                ],
            ]
        ];

        return view('backend.admin.edit', compact(
            'user',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $__request = $request->all();

        $user = \App\Models\Admin::findOrFail($id);

        
        $user->status = isset($__request["status"]) ? 1 : 0;

        $update = $user->save();

        if ($update) {
            return redirect('backend/user')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/user')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \App\Models\Admin::findOrFail($id);

        if ($user->delete()) {
            session()->flash('success', 'Xóa thành công #' . $id);

            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công #' . $id,
            ]);
        } else {
            session()->flash('error', 'Không thể xóa#' . $id);

            return response()->json([
                'error' => 1,
                'message' => 'Không thể xóa#' . $id,
            ]);
        }
    }
}