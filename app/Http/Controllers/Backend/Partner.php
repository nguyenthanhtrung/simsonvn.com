<?php

namespace App\Http\Controllers\Backend;

use App\Common\Utility;
use App\Models\HomePostGroup;
use App\Models\PostCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Partner extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            'title' => 'Danh sách Đối tác',
            'items' => [
                [
                    'title' => 'Danh sách Đối tác',
                    'url' => '/backend/partner'
                ]
            ]
        ];

        $data = \App\Models\Partner::query()->orderBy('created_time', 'desc')
            ->orderBy('created_time', 'desc')->paginate(24);

        return view('backend.partner.index', compact('data', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Thêm Đối tác',
            'items' => [
                [
                    'title' => 'Danh sách Đối tác',
                    'url' => '/backend/partner'
                ],
                [
                    'title' => 'Thêm Đối tác'
                ]
            ]
        ];

        return view('backend.partner.create', compact(
            'post_categories',
            'home_groups',
            'breadcrumb'
        ));
    }


    public function store(Request $request)
    {

        $user = auth()->user();

        $data = new \App\Models\Partner();

        $name = $request->get('name', '');
        $introduce = $request->get('introduce', '');
        $url = $request->get('url', '');
        $order = $request->get('order', '');
        $status = $request->get('status', '');

        if ($status == 'on') {
            $status = 1;
        } else {
            $status = 0;
        }

        $data->name = $name;
        $data->introduce = $introduce;
        $data->url = $url;
        $data->updated_by_id = $user->id;
        $data->updated_by_name = $user->username;
        $data->status = $status;
        $data->order = $order;

        if ($data->save()) {
            if (Input::hasFile('thumb')) {
                $image = Input::file('thumb');

                $file = ['image' => $image];
                $validator = Validator::make($file, [
                    'image' => 'mimes:jpeg,bmp,png|max:1000'
                ]);
                if ($validator->fails()) {
                    return redirect()->action('Backend\Partner@edit', ['id' => $data->id])->withInput()->withErrors($validator);
                }

                $fileName = $data->id . '.jpg';

                $filePath = env('UPLOAD_PATH') . '/partner/';

                $image->move($filePath, $fileName);

                $data->image = env('WEB_PATH') . '/upload/partner/' . $fileName;;
                $data->save();
            }

            return redirect('backend/partner')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/partner')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = \App\Models\Partner::findOrFail($id);

        $breadcrumb = [
            'title' => $data->name,
            'items' => [
                [
                    'title' => 'Danh sách Đối tác',
                    'url' => '/backend/partner'
                ],
                [
                    'title' => 'Xem chi tiết',
                    'url' => route('post.show', ['id' => $data])
                ],
            ]
        ];

        return view('backend.partner.edit', compact(
            'data',
            'breadcrumb'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();

        $name = $request->get('name', '');
        $introduce = $request->get('introduce', '');
        $url = $request->get('url', '');
        $order = $request->get('order', '');
        $status = $request->get('status', '');

        if ($status == 'on') {
            $status = 1;
        } else {
            $status = 0;
        }

        $data = \App\Models\Partner::findOrFail($id);
        $data->name = $name;
        $data->introduce = $introduce;
        $data->url = $url;
        $data->updated_by_id = $user->id;
        $data->updated_by_name = $user->username;
        $data->status = $status;
        $data->order = $order;

        if ($data->save()) {
            if (Input::hasFile('thumb')) {
                $image = Input::file('thumb');

                $file = ['image' => $image];
                $validator = Validator::make($file, [
                    'image' => 'mimes:jpeg,bmp,png|max:1000'
                ]);
                if ($validator->fails()) {
                    return redirect()->action('Backend\Partner@edit', ['id' => $data->id])->withInput()->withErrors($validator);
                }

                $fileName = $data->id . '.jpg';

                $filePath = env('UPLOAD_PATH') . '/partner/';

                $image->move($filePath, $fileName);

                $data->image = env('WEB_PATH') . '/upload/partner/' . $fileName;;
                $data->save();
            }

            return redirect('backend/partner')->with('success', 'Cập nhật thành công');
        }

        return redirect('backend/partner')->with('error', 'Có lỗi xảy ra!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = \App\Models\Partner::findOrFail($id);

        if ($data->delete()) {
            session()->flash('success', 'Xóa thành công Đối tác #' . $id);

            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công Đối tác #' . $id,
            ]);
        } else {
            session()->flash('error', 'Không thể xóa Đối tác #' . $id);

            return response()->json([
                'error' => 1,
                'message' => 'Không thể xóa Đối tác #' . $id,
            ]);
        }
    }

}