<?php
namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;
use Closure;
class BackendAuthenticate extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next, ...$guards)
    {
        if(!auth()->check()) {
            return redirect()->to(url('backend/login'));
        }

        return $next($request);
    }
}
