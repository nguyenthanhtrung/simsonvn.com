<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRequireActive extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function handle()
    {
	    throw new \Exception('cant not run queue');

    }

    public function fire() {

    }
}
