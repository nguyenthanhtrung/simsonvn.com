<?php
//Route::get('/test', '\App\Http\Controllers\Frontend\Test@index');
Route::get('/', '\App\Http\Controllers\Frontend\Home@index');
Route::get('/san-pham/{slug}.html', '\App\Http\Controllers\Frontend\Product@detail');

//Route::get('/gioi-thieu', 'Frontend\Post@show');
//Route::get('/lien-he', 'Frontend\Post@show');

Route::get('/bai-viet/{slug}/{id}.html', '\App\Http\Controllers\Frontend\Post@show');



Route::get('/danh-muc/{slug}', 'Frontend\Post@index');
Route::get('/tim-kiem', 'Frontend\Product@search');
Route::post('/order/create', 'Frontend\Order@create');


$parents = \App\Models\ProductCategory::query()->where('parent_id',0)->get();
$parents_slugs = [];
if(isset($parents) && !empty($parents)) {
	foreach($parents as $parent) {
		$parents_slugs[] = $parent->slug;
	}
}
Route::get('/{category_slug}/{p1?}/{p2?}',
    '\App\Http\Controllers\Frontend\Product@index')
    ->where(['category_slug' => implode('|',$parents_slugs)]);