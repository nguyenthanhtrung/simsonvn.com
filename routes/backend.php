<?php


Route::get('test/update_account_balance', '\App\Http\Controllers\Test@updateAccountBalance');
//
Route::get('login', 'Backend\Login@index');

Route::post('login', 'Backend\Login@login');
Route::get('logout', 'Backend\Login@logout');

Route::group(['middleware' => 'auth.backend'], function () {

    Route::post('/image/editor-upload', 'Backend\Image@editorUpload');

    Route::get('', 'Backend\Home@index');
    Route::get('home', 'Backend\Home@index');
    Route::resource('admin', 'Backend\Admin');
    Route::resource('post_category', 'Backend\PostCategory');

    Route::post('order/change_status', 'Backend\Order@changeStatus');
    Route::get('order/delete', 'Backend\Order@delete');
    Route::get('order', 'Backend\Order@index');

    #bài viết
    Route::post('/image/editor-upload', 'Backend\Image@editorUpload');
    Route::get('post/delete/{id}', 'Backend\Post@delete');
    Route::resource('post', 'Backend\Post');

    #video
    Route::get('video/delete/{id}', 'Backend\Video@delete');
    Route::resource('video', 'Backend\Video');

    #danh mục sản phẩm
    Route::get('product_category/delete/{id}', 'Backend\ProductCategory@delete');
    Route::resource('product_category', 'Backend\ProductCategory');

    #thương hiệu sản phẩm
    Route::get('product_brand/delete/{id}', 'Backend\ProductBrand@delete');
    Route::resource('product_brand', 'Backend\ProductBrand');


	Route::get('product_price/list', 'Backend\ProductPrice@lists');
	Route::post('product_price/add', 'Backend\ProductPrice@add');
	Route::post('product_price/remove', 'Backend\ProductPrice@remove');

	Route::get('product_promotion/list', 'Backend\ProductPromotion@lists');
	Route::post('product_promotion/add', 'Backend\ProductPromotion@add');
	Route::post('product_promotion/remove', 'Backend\ProductPromotion@remove');

    Route::post('product_it/add', 'Backend\ProductIt@add');
    
    Route::post('product_image/add', 'Backend\ProductImage@add');
    Route::post('product_image/remove', 'Backend\ProductImage@remove');
    Route::get('product_image/lists', 'Backend\ProductImage@lists');


    Route::get('product/delete/{id}', 'Backend\Product@delete');
    Route::resource('product', 'Backend\Product');
    
    Route::get('banner/delete/{id}', 'Backend\Banner@delete');
    Route::resource('banner', 'Backend\Banner');

	Route::get('banner_sub/delete/{id}', 'Backend\BannerSub@delete');
    Route::resource('banner_sub', 'Backend\BannerSub');
	
    Route::resource('system_config', 'Backend\SystemConfig');

	Route::get('partner/delete/{id}', 'Backend\Partner@delete');
	Route::resource('partner', 'Backend\Partner');
});
