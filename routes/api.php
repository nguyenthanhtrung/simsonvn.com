<?php
Route::post('account/synchronize', 'Api\Account@synchronize');
Route::post('transaction/synchronize', 'Api\Transaction@synchronize');

Route::get('account','Api\Account@show');
Route::get('account/{id}/balance','Api\Account@getBalance');
Route::get('account/balance','Api\Account@getBalanceFromParams');